<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recharge extends Model
{
     protected $primaryKey = "recreportid";
     protected $table="recharge_report";
}
