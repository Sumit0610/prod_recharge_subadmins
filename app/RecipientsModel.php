<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipientsModel extends Model
{
    //
    protected $table = "eko_recipients";
}
