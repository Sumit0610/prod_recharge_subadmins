<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EkoUserModel extends Model
{
    //
    protected $primaryKey = "sender_id";
    protected $table = "eko_sender";
}
