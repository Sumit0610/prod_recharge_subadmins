<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image as Image;
use App\User;
use App\AboutModel;
use App\ServiceModel;
use Validator;
use App\ContactModel;
use App\RecipientsModel;
use App\TransactionsModel;
use App\Recharge;
use App\SuperAdminModel;


class UserController extends Controller
{
    //
	public function __construct() {
        $this->middleware('auth');
    }

    public function home(Request $request) {

        $newsdata = SuperAdminModel::all();
        return view('admin_rw.home')->with('newsdata', $newsdata);

    }

    public function money_transfer(Request $request) {

    	$datas = $request->session()->all();

    	$id = $datas["login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d"];

        $userdata = User::where('id', $id)->first();

    	$recipient_data = RecipientsModel::where('userid', $id)->get();

    	return view('subadmins_rw.money_transfer')->with('recipient_data', $recipient_data)->with('userdata', $userdata);
    }


    public function about_add() {

		$datas = session()->all();
		$id =  $datas['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
        $i = 1;
        $recharge_data = Recharge::get();


        return view('admin_rw.about_us')->with('recharge_data', $recharge_data)->with('i',$i);
    	//return view('admin_rw.about_us')->with('about_data', $about_data);
    }

    public function save_about_add() {
		$attributeNames = array(
		    'aboutusdesc'             => 'About Description'
		);
		$rules = array(
		    'aboutusdesc'             => 'required|min:20'
		);

		$validator = Validator::make(Input::all(), $rules);
		$validator->setAttributeNames($attributeNames);
		if ($validator->fails()) {

		    $messages = $validator->messages();
		    return Redirect::to('/admin/about_add')
		        ->withErrors($validator);

		} else {
		    $data = session()->all();
		    $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];

	    	$about_data = AboutModel::where('user_id', $id)->first();

	    	$aboutusdesc = Input::get('aboutusdesc');

		    if(isset($about_data->about_id)){
		    	 $about1 = AboutModel::where('user_id', $id)->update(['about_desc' => $aboutusdesc]);
		    }
		    else{
		    	 $about1 = new AboutModel;
				 $about1->about_desc = Input::get('aboutusdesc');
				 $about1->user_id = $id;
				 $about1->save();	
		    }
		   
		    
		}

		return redirect()->back()->with('status', 'About Description data has been saved successfully');
    }


    public function service_add() {
    	return view('admin_rw.services');
    }

    public function save_service_add(Request $req) {
		$data = $req->image;

		list($type, $data) = explode(';', $data);
		list(, $data)      = explode(',', $data);

		$datas = session()->all();
		$id =  $datas['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];

		$data = base64_decode($data);
		$image_name = time(). 'service_img-' . $id .'.png';
		$path = public_path() . "/uploads//services//" . $image_name;

		file_put_contents($path, $data);

		$service_data = new ServiceModel;
		$service_data->service_name = Input::get('tnm');
		$service_data->service_desc = Input::get('tps');   
		$service_data->service_desc_detail = Input::get("service_brief");            
		$service_data->service_img = $image_name;
		$service_data->u_id = $id;
		$service_data->save();
    }

    public function service_update(){

		$datas = session()->all();
		$id =  $datas['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];

		$service_data = ServiceModel::where('u_id', $id)->get();

		return view('admin_rw.services_update')->with('service_data', $service_data);

    }

    public function save_service_update(){

    	 $rules = array(
                'teammemnm'             => 'nullable|min:2',
                'teammempos'             => 'nullable|min:2',
                'service_desc_detail'             => 'nullable|min:2'  
            );


            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails()) {

                $messages = $validator->messages();
                return Redirect::to('/admin/about_list')
                    ->withErrors($validator);

            } else {
                    
               	$data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
                $service_data = ServiceModel::where('id',Input::get('id'))->first();
                $service_data->service_name     = Input::get('teammemnm');
                $service_data->service_desc     = Input::get('teammempos');
                $service_data->service_desc_detail     = Input::get('service_desc_detail');
                $service_data->save();

                
                return redirect()->back()->with('status', 'Service details updated successfully' );
        }

     }   


     public function edit_service_photo(){
     	$service_data = ServiceModel::where('id', Input::get("id"))->first();
     	return view('admin_rw.edit_service_photo')->with('id', Input::get("id"))->with('service_data', $service_data);
     }

     public function edit_service_photo_save(Request $request){
     	 $data = $request->image;
        $id = Input::get('id');

        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);

        $data = base64_decode($data);
        $image_name = time(). '-' . $id .'.png';
        $path = public_path() . "/uploads//services//" . $image_name;

        file_put_contents($path, $data);

        $userdata = ServiceModel::where('id',$id)->update(['service_img'=>$image_name]);

        return response()->json(['success'=>'done']);
     }

     public function service_delete(){

		$data = session()->all();
		$id = $data["login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d"];
		$service_data = ServiceModel::where('u_id', $id)->get();

		return view('admin_rw.delete_service')->with('service_data', $service_data);

     }

     public function save_service_delete(Request $request){
     	ServiceModel::where('id', Input::get("id"))->delete();
     	return redirect()->back()->with('status', 'Service deleted successfully');
     }


     public function view_contacts(){
     	$data_contactus = ContactModel::all();
     	return view('admin_rw.view_contacts')->with('data_contactus', $data_contactus);
     }

     public function add_recipients(Request $request) {
     	// $url = "https://staging.eko.co.in:25004/ekoapi/v1/customers/";   //Preprod URL
        $url = "https://api.eko.co.in:25002/ekoicici/v1/customers/";   	//Prod URL

     	$data = $request->session()->all();

     	$ids = $data["login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d"];

     	$userdata = User::where('id', $ids)->first();
		// $dev_key = 'becbbce45f79c6f5109f848acd540567';    //Preprod credentials
		// $init_id = '9910028267';

		$dev_key = '90beb0087a11fd0361b47d1d5ab77ec5';    //Prod credentials
		$init_id = '9923081299';

        $key = 'e9de289f-8865-4305-9765-aad144e98f74';
        $encodedKey = base64_encode($key);

        $secret_key_timestamp = round(microtime(true) * 1000); 

        $signature = hash_hmac('SHA256', $secret_key_timestamp, $encodedKey, true);

        $secret_key = base64_encode($signature);

		$customer_id_type = 'mobile_number';
		$cust_id = $userdata->user_phone; //Logged in user no!
		$recipient_id_type = 'acc_ifsc';
		$recipient_type = '3';
		$ifsc_code = Input::get("ifsc_code");
		$recipient_name = Input::get("rec_name");
		$recipient_mobile = Input::get("rec_mobile");
		$account_number = Input::get("account_number");

		$id = $account_number . "_" . $ifsc_code;


		$url = $url . $customer_id_type . ":" . $cust_id . "/recipients/" . $recipient_id_type . ":" . $id;

		$bodyParam = "customer_id_type=" . $customer_id_type . "&customer_id= " . $cust_id . "&recipient_type=" . $recipient_type;
			$bodyParam = $bodyParam . "&recipient_id_type=" . $recipient_id_type ."&id=" . $id . "&recipient_name=" . $recipient_name;
			$bodyParam = $bodyParam . "&recipient_mobile=" . $recipient_mobile . "&initiator_id=" . $init_id;
	
	
 try {
        $curl = curl_init();

        if (FALSE === $curl)
        throw new Exception('failed to initialize');

            curl_setopt_array($curl, array(
            CURLOPT_PORT => "25002",
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_POSTFIELDS => $bodyParam,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded",
            "developer_key:" . $dev_key,
            "secret-key:" . $secret_key,
            "secret-key-timestamp:" . $secret_key_timestamp
            ),
            ));

        $response = curl_exec($curl);

        $json_retrieve = json_decode($response, false);

        if($json_retrieve->status == 0) {

        	$recipient_data = new RecipientsModel;
        	$recipient_data->rec_name = Input::get("rec_name");
        	$recipient_data->rec_mobile = Input::get("rec_mobile");
        	$recipient_data->account_number = Input::get("account_number");
        	$recipient_data->ifsc_code = Input::get("ifsc_code");
        	$recipient_data->userid = $ids;
        	$recipient_data->eko_recipient_id = $json_retrieve->data->recipient_id;
        	$recipient_data->save();

        	return redirect()->back()->with('status', 'Benificiary added successfully!');
           
        }
        else {

        	return redirect()->back()->with('error', 'Error adding benificiary!');
        }

        if (FALSE === $response)

            throw new \Exception(curl_error($curl), curl_errno($curl));

        } catch(\Exception $e) {

            trigger_error(sprintf(
            'Curl failed with error #%d: %s',
            $e->getCode(), $e->getMessage()),
            E_USER_ERROR);

        }
     }


     public function send_money(Request $request) {

     	$datass = $request->session()->all();

     	$ids = $datass["login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d"];

     	$userdatas = User::where('id', $ids)->first();

        // $userdata = RecipientsModel::where('rec_mobile', Input::get("rec_mobile"))->where('userid',$ids)->first();

     	// if(isset($userdata)) {

				$url = "https://staging.eko.co.in:25004/ekoapi/v1/transactions"; //Preprod URL
     			// $url = "https://api.eko.co.in:25002/ekoicici/v1/transactions"; //Prod URL

				$milliseconds = round(microtime(true) * 1000);

				$recipient_id = Input::get('eko_recipient_id');
				$amount = Input::get("amount");
				$timestamp = $milliseconds;
				$currency = 'INR';
				$customer_id = Input::get('sender_mobile');
				// $init_id = '9910028267';
				$client_ref_id = $recipient_id . '_' . str_random(2);
                echo $client_ref_id;
				$hold_timeout = '30';
				$state = '1';
				$auth_type = 'otp';
				$auth = '5000';
				$channel = '2';
				$currency_name = "currency";
				// $dev_key = 'becbbce45f79c6f5109f848acd540567';
				$dev_key = 'becbbce45f79c6f5109f848acd540567';    //Preprod credentials
				$init_id = '9910028267';

                // $dev_key = '90beb0087a11fd0361b47d1d5ab77ec5';      //Prod credentials
                 $key = 'e9de289f-8865-4305-9765-aad144e98f74';
                $encodedKey = base64_encode($key);

                $secret_key_timestamp = round(microtime(true) * 1000); 

                $signature = hash_hmac('SHA256', $secret_key_timestamp, $encodedKey, true);
                 
                $secret_key = base64_encode($signature);
				$init_id = '9923081299';

				$pincode = '440009';
				$merchant_document_id = 'BFPPG2626M';
				$merchant_document_id_type = '1';

				$bodyParam = "recipient_id=" . $recipient_id . "&amount=" . $amount . "&amp;timestamp=:" . $timestamp . "&amp;" . $currency_name . "=" . $currency;
				$bodyParam = $bodyParam . "&customer_id=" . $customer_id . "&initiator_id=" . $init_id . "&client_ref_id=" . $client_ref_id;
				$bodyParam = $bodyParam . "&hold_timeout=" . $hold_timeout . "&state=" . $state . "&" . $auth_type . "=" . $auth . "&channel=" . $channel . "&pincode=" . $pincode . "&merchant_document_id=" . $merchant_document_id . "&merchant_document_id_type=" . $merchant_document_id_type;	

		     	try {
        $curl = curl_init();

        if (FALSE === $curl)
        throw new Exception('failed to initialize');

            curl_setopt_array($curl, array(
            CURLOPT_PORT => "25004",
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_POSTFIELDS => $bodyParam,
            CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded",
            "developer_key:" . $dev_key
            ),
            ));

        $response = curl_exec($curl);

        $json_retrieve = json_decode($response, false);

        // var_dump($json_retrieve);
        // var_dump($response);

        if($json_retrieve->status == 0) {

        	$transcationdata = new TransactionsModel;
        	$transcationdata->eko_recipient_id = $recipient_id;
        	$transcationdata->user_id = $ids;
        	$transcationdata->fee = $json_retrieve->data->fee;
        	$transcationdata->collectable_amount = $json_retrieve->data->collectable_amount;
        	$transcationdata->tid = $json_retrieve->data->tid;
        	$transcationdata->balance = $json_retrieve->data->balance;
        	$transcationdata->bank_ref_num = $json_retrieve->data->bank_ref_num;
        	$transcationdata->recipient_id = $json_retrieve->data->recipient_id;
        	$transcationdata->amount = $json_retrieve->data->amount;
        	$transcationdata->channel_desc = $json_retrieve->data->channel_desc;
        	$transcationdata->customer_id = $json_retrieve->data->customer_id;
            $transcationdata->account_number = $json_retrieve->data->account;
            $transcationdata->verification_type = Input::get('verification');
        	$transcationdata->verification_number = Input::get('verification_number');

        	$transcationdata->save();

        	return redirect()->back()->with('status', 'Money sent successfully!');
        }
        else {
        	$transcationdata = new TransactionsModel;
        	$transcationdata->eko_recipient_id = $recipient_id;
        	$transcationdata->user_id = $ids;
        	$transcationdata->amount = $amount;
        	$transcationdata->save();
            if(isset($json_retrieve->message)){
        	   return redirect()->back()->with('error', $json_retrieve->message);
            }
            else{
               return redirect()->back()->with('error', 'Error sending money!');                
            }
        }

        if (FALSE === $response)

            throw new \Exception(curl_error($curl), curl_errno($curl));

        } catch(\Exception $e) {

            trigger_error(sprintf(
            'Curl failed with error #%d: %s',
            $e->getCode(), $e->getMessage()),
            E_USER_ERROR);

        }
		    // }

		    // else{
		    // 	return redirect()->back()->with('error', 'Mobile number not added as benificiary yet!');
		    // }

     }



     public function recharge_phone(Request $request) {
         $datass = $request->session()->all();

        $ids = $datass["login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d"];

        $userdata = User::where('id', $ids)->first();
        $user_verified = $userdata->is_verified;

     	return view('subadmins_rw.recharge_phone')->with('ids', $ids)->with('user_verified', $user_verified);
     }


    public function new_old_user(Request $request) {
        $datass = $request->session()->all();

        $ids = $datass["login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d"];

        $userdatas = User::where('id', $ids)->first();

        if($userdatas->is_verified == 1){

            if($userdatas->iseko_verified == 1){
                    return redirect()->back()->with('status', 'Already verified!');    
            }
            else {
                return view('subadmins_rw.login_register')->with('userdatas', $userdatas);           
            }

        }
        else {
            return redirect()->back()->with('error', 'Contact APG Online Services and get your account verified first!');
        }

    }

    public function save_website_user(Request $request) {

        $id_type = 'mobile_number';
        $id = Input::get("mobile_no");
        // $init_id = '9910028267';
        $name = Input::get("username");
        // $secret_key = 'Z86hswK9xE4Ap8kQV7i++JtEQV1tKSOMfjfYYCpG0='; 
        // $secret_key_timestamp = round(microtime(true) * 1000); 

        $key = 'e9de289f-8865-4305-9765-aad144e98f74';
        $encodedKey = base64_encode($key);

        $secret_key_timestamp = round(microtime(true) * 1000); 

        $signature = hash_hmac('SHA256', $secret_key_timestamp, $encodedKey, true);

        $secret_key = base64_encode($signature);

        // $dev_key = 'becbbce45f79c6f5109f848acd540567';

        // $dev_key = 'becbbce45f79c6f5109f848acd540567';    //Preprod credentials
        // $init_id = '9910028267';

        $dev_key = '90beb0087a11fd0361b47d1d5ab77ec5';    //Prod credentials
        $init_id = '9923081299';

        // $url = "https://staging.eko.co.in:25004/ekoapi/v1/customers/";       //Preprod URL
           $url = "https://api.eko.co.in:25002/ekoicici/v1/customers/";
     $url = $url . $id_type . ":" . $id;

        $bodyParam = "customer_id_type=" . $id_type . "&id=" . $id . "&initiator_id=" . $init_id . "&name=" . $name; 

        try {
        $curl = curl_init();

        if (FALSE === $curl)
        throw new Exception('failed to initialize');

            curl_setopt_array($curl, array(
            CURLOPT_PORT => "25002",
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_POSTFIELDS => $bodyParam,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded",
            "developer_key:" . $dev_key,
            "secret-key:" . $secret_key,
            "secret-key-timestamp:" . $secret_key_timestamp
            ),
            ));

        $response = curl_exec($curl);

        $json_retrieve = json_decode($response, false);

        var_dump($response);
        var_dump($json_retrieve);


        if($json_retrieve->status == 0) {

            $request->session()->put('name', Input::get("username"));
            $request->session()->put('email', Input::get("email"));
            $request->session()->put('user_phone', Input::get("mobile_no"));
            $request->session()->put('password', Input::get("password"));
            //$request->session()->put('otp', $json_retrieve->data->otp);     // not sent in prod
            $request->session()->put('otp', "prod");

            return redirect('admin/enter_otp');

        }
        else {
             return redirect()->back()->with('error', $json_retrieve->message);
        }
        

        if (FALSE === $response)

            throw new \Exception(curl_error($curl), curl_errno($curl));

        } catch(\Exception $e) {

            trigger_error(sprintf(
            'Curl failed with error #%d: %s',
            $e->getCode(), $e->getMessage()),
            E_USER_ERROR);

        }

        // $website_user_data = new SubAdminModel;
        // $website_user_data->name = Input::get("username");
        // $website_user_data->email = Input::get("email");
        // $website_user_data->user_phone = Input::get("mobile_no");
        // $website_user_data->password = bcrypt(Input::get("password"));
        // $website_user_data->save();

        // return redirect()->back()->with('status', 'User registered succesfully!');
    }

    public function enter_otp(Request $request) {

                 $data = $request->session()->all();
    
                  // echo $data["otp"];

         return view('subadmins_rw.enter_otp');
    }

     public function verify_user(Request $request) {


           $data = $request->session()->all();

        $name = $data["name"];
        $email = $data["email"];
        $user_phone = $data["user_phone"];
        $password = $data["password"];

        // $url = "https://staging.eko.co.in:25004/ekoapi/v1/customers/verification/otp:";  //Preprod URL

        $url = "https://api.eko.co.in:25002/ekoicici/v1/customers/verification/otp:";

        $otp =Input::get("otp");
        $id = $user_phone;
            // $dev_key = 'becbbce45f79c6f5109f848acd540567';    //Preprod credentials
            // $init_id = '9910028267';

        $dev_key = '90beb0087a11fd0361b47d1d5ab77ec5';    //Prod credentials

        $init_id = '9923081299';
        $id_type = 'mobile_number';

        $key = 'e9de289f-8865-4305-9765-aad144e98f74';
        $encodedKey = base64_encode($key);

        $secret_key_timestamp = round(microtime(true) * 1000); 

        $signature = hash_hmac('SHA256', $secret_key_timestamp, $encodedKey, true);

        $secret_key = base64_encode($signature);

        $url = $url . $otp;

         $bodyParam = "id=" . $id . "&id_type=" . $id_type . "&otp=" . $otp . "&initiator_id=" . $init_id;
            
    
        try {
        $curl = curl_init();

        if (FALSE === $curl)
        throw new Exception('failed to initialize');

            curl_setopt_array($curl, array(
            CURLOPT_PORT => "25002",
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_POSTFIELDS => $bodyParam,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded",
            "developer_key:" . $dev_key,
            "secret-key:" . $secret_key,
            "secret-key-timestamp:" . $secret_key_timestamp
            ),
            ));

        $response = curl_exec($curl);

        $json_retrieve = json_decode($response, false);


        if($json_retrieve->status == 0) {

            // $website_user_data = new SubAdminModel;
            // $website_user_data->name = $name;
            // $website_user_data->email = $email;
            // $website_user_data->user_phone = $user_phone;
            // $website_user_data->password = bcrypt($password);
            // $website_user_data->save();

            $datass = $request->session()->all();

            $ids = $datass["login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d"];

            $userdatas = User::where('id', $ids)->update(['iseko_verified' => 1]);

            return redirect('admin/money_transfer')->with('status', 'Verified successfully!');
        }
        else {
            return redirect()->back()->with('error', $json_retrieve->message);
        }


        if (FALSE === $response)

            throw new \Exception(curl_error($curl), curl_errno($curl));

        } catch(\Exception $e) {

            trigger_error(sprintf(
            'Curl failed with error #%d: %s',
            $e->getCode(), $e->getMessage()),
            E_USER_ERROR);

        }


    }


    public function add_user(Request $request) {
        $datass = $request->session()->all();

        $ids = $datass["login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d"];

        $userdata = User::where('id', $ids)->first();
        $user_verified = $userdata->is_verified;
        return view('admin_rw.add_user')->with('userdata', $userdata);
    }

    public function view_user(Request $request) {
        $datass = $request->session()->all();

        $ids = $datass["login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d"];

        $subadmindata = User::where('creatorid', $ids)->get();

        return view('admin_rw.view_user')->with('view_users', $subadmindata);
    }


    public function save_subadmin_user(Request $request) {

        $id_type = 'mobile_number';
        $user_phone = Input::get("mobile_no");
        // $init_id = '9910028267';
        $name = Input::get("username");
        $email = Input::get("email");
        $password = Input::get("password");
        $user_type = Input::get("type_customer");
        $panno = Input::get("panno");
        $address = Input::get("address");
        $creatorid = Input::get("userid");

        $subadmindata = User::where('email', $email)->orWhere('user_phone', $user_phone)->first();

        if(!isset($subadmindata->id)) {
            if($user_type == 'NA') {
                return redirect()->back()->with('error', 'Please select a valid user type');
            }
            else {
                $website_user_data = new User;
                $website_user_data->name = $name;
                $website_user_data->email = $email;
                $website_user_data->user_phone = $user_phone;
                $website_user_data->password = bcrypt($password);
                $website_user_data->user_type = $user_type;
                $website_user_data->address = $address;
                $website_user_data->panno = $panno;
                $website_user_data->is_verified = 1;
                $website_user_data->creatorid = $creatorid;
                $website_user_data->save();
                return redirect()->back()->with('status', 'User added successfully!');
            }
        }
        else {
            return redirect()->back()->with('error', 'User Email/Mobile No already exists!');
        }

    }

    public function checkuserfirst(Request $req)
    {
        $datas = $req->session()->all();
            $id = $datas["login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d"];
            $userdata = User::where('id', $id)->first();
        $mobile = Input::get('rec_mobile');
        $dev_key = "becbbce45f79c6f5109f848acd540567";
        $init_id = "9910028267";
        $cust_id_type = "mobile_number";
        $cust_id = $mobile;
        $url = "https://staging.eko.co.in:25004/ekoapi/v1/customers/";
        $url = $url . $cust_id_type . ":" . $cust_id . "?initiator_id=" . $init_id;  //concatenating url parameters with url parameters
        //echo $url;
        if(isset($mobile) && strlen($mobile) >=10)
        {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "25004",
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "developer_key: ".$dev_key
            ),
            ));

        $response = curl_exec($curl); // Contains the response from server
        //echo "<br>";
        //echo $response;
        //echo "<br>";
        $myjson = json_decode($response);
        $checkexist = $myjson->response_status_id;
        if($checkexist == 0)
        {
            //echo "Sender Exist <br>";
            $url = "https://staging.eko.co.in:25004/ekoapi/v1/customers/";
            $url = $url . $cust_id_type . ":" . $cust_id . "/recipients?initiator_id=" . $init_id; ////concatenating url with url parameters
                
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_PORT => "25004",
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "developer_key: ".$dev_key
                ),
                ));

            $response1 = curl_exec($curl); // Contains the response from server
            $myjson1 = json_decode($response1);
            $checkexist1 = $myjson1->response_status_id;
            
            if($checkexist1 == 0)
            {
                //echo "Recipient existed";echo "<br>";
                foreach($myjson1->data->recipient_list as $recipient)
                {
                    // echo $recipient->recipient_mobile; echo "<br>";
                    // echo $recipient->recipient_name;  echo "<br>";
                    // echo $recipient->account;  echo "<br>";
                    // echo $recipient->ifsc;  echo "<br>";
                }
                
                // $recipient_data = RecipientsModel::where('userid', $id)->get();
                return view('subadmins_rw.money_transfer')->with('recipient_data',$myjson1->data->recipient_list)->with('userdata',$userdata)->with('sendercontact',Input::get('rec_mobile'));
            }else
            {
                //echo "Recipient does not existed";
                return view('subadmins_rw.money_transfer')->with('recipient_data',null)->with('userdata',$userdata);
            }
        }
        else
        {
            return view('subadmins_rw.money_transfer')->with('status1',"Sender does not exist")->with('userdata',$userdata);
        }
    }else{
        return view('subadmins_rw.money_transfer')->with('status1',"Please fill the details first to send money")->with('userdata',$userdata);
    }
        //IFSC Code: CORP0001216
    
    }   

    
    public function view_userdata(Request $request) {
        $datass = $request->session()->all();

        $ids = $datass["login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d"];

        $recipient_name = $request->recipient_name;
        $reccontact = $request->recipient_contact;
        $sendercontact = $request->sendercontact;
        $recipient_id = $request->recipient_id;
        return view('admin_rw.view_userdata')->with('reccontact', $reccontact)->with('sendercontact', $sendercontact)->with('recipient_name', $recipient_name)->with('recipient_id', $recipient_id);
    }

}
