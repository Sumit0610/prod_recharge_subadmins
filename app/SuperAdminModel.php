<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuperAdminModel extends Model
{
    //
    protected $connection = "mysql2";
    protected $table = "latest_news";
}
