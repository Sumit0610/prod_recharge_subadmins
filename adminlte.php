<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | The default title of your admin panel, this goes into the title tag
    | of your page. You can override it per page with the title section.
    | You can optionally also specify a title prefix and/or postfix.
    |
    */

    'title' => 'Admin Console',

    'title_prefix' => '',

    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | This logo is displayed at the upper left corner of your admin panel.
    | You can use basic HTML here if you want. The logo has also a mini
    | variant, used for the mini side bar. Make it 3 letters or so
    |
    */

    'logo' => '<b>Dashboard</b>',

    'logo_mini' => '<b>D</b>B',

    /*
    |--------------------------------------------------------------------------
    | Skin Color
    |--------------------------------------------------------------------------
    |
    | Choose a skin color for your admin panel. The available skin colors:
    | blue, black, purple, yellow, red, and green. Each skin also has a
    | ligth variant: blue-light, purple-light, purple-light, etc.
    |
    */

    'skin' => 'blue',

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Choose a layout for your admin panel. The available layout options:
    | null, 'boxed', 'fixed', 'top-nav'. null is the default, top-nav
    | removes the sidebar and places your menu in the top navbar
    |
    */

    'layout' => null,

    /*
    |--------------------------------------------------------------------------
    | Collapse Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we choose and option to be able to start with a collapsed side
    | bar. To adjust your sidebar layout simply set this  either true
    | this is compatible with layouts except top-nav layout option
    |
    */

    'collapse_sidebar' => false,

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Register here your dashboard, logout, login and register URLs. The
    | logout URL automatically sends a POST request in Laravel 5.3 or higher.
    | You can set the request to a GET or POST with logout_method.
    | Set register_url to null if you don't want a register link.
    |
    */

    'dashboard_url' => 'home',

    'logout_url' => 'logout',

    'logout_method' => null,

    'login_url' => 'login',

    'register_url' => 'register',

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Specify your menu items to display in the left sidebar. Each menu item
    | should have a text and and a URL. You can also specify an icon from
    | Font Awesome. A string instead of an array represents a header in sidebar
    | layout. The 'can' is a filter on Laravel's built in Gate functionality.
    |
    */

    'menu' => [
        'MAIN NAVIGATION',
        [
            'text'        => 'Website URL',
            'url'         => 'admin/web_url',
            'icon'        => 'file',
            'label_color' => 'success',
        ],
        
         [
            'text'        => 'Template',
            'url'         => 'admin/template',
            'icon'        => 'file'
        ],
        [
            'text'        => 'Publish',
            'url'         => 'admin/publish',
            'icon'        => 'file'
        ],
        [
            'text' => 'Blog',
            'url'  => 'admin/blog',
            'can'  => 'manage-blog',
        ],
        [
            'text'        => 'Logo',
            'url'         => 'admin/logo',
            'icon'        => 'file',
            'label_color' => 'success',
        ],
         [
            'text'        => 'Slider',
            'url'         => 'admin/slider',
            'icon'        => 'file',
        ],
       
        [
            'text'        => 'About Us',
            'url'         => 'admin/about_us',
            'icon'        => 'file',
            'submenu' => [
                [
                    'text' => 'Add About Details',
                    'url'  => 'admin/about_us',
                ],
                [
                    'text' => 'Edit About Details',
                    'url'  => 'admin/about_list',
                ],
                [
                    'text' => 'Delete About Details',
                    'url'  => 'admin/delabout',
                ],

            ]
            // 'label'       => 4,
            // 'label_color' => 'success',
        ],
        [
            'text'        => 'Vision / Mission',
            'url'         => 'admin/',
            'icon'        => 'file',
            'submenu' => [
                [
                    'text' => 'Add Vision and Mission',
                    'url'  => 'admin/add_vmission',
                ],
                [
                    'text' => 'Edit vision and mission Details',
                    'url'  => 'admin/vmission_list',
                ],
                [
                    'text' => 'Delete vision and mission Details',
                    'url'  => 'admin/delvm_list',
                ]
            ]
            // 'label'       => 4,
            // 'label_color' => 'success',
        ],
        [
            'text'        => 'Our Works',
            'url'         => 'admin/add_ourworks',
            'icon'        => 'file',
            'submenu' => [
                [
                    'text' => 'Add Vision and Mission',
                    'url'  => 'admin/add_ourworks',
                ],
                [
                    'text' => 'Edit vision and mission Details',
                    'url'  => 'admin/edit_ourworks',
                ],
                [
                    'text' => 'Delete vision and mission Details',
                    'url'  => 'admin/del_ourworks',
                ]
            ]
            // 'label'       => 4,
            // 'label_color' => 'success',
        ],
        [
            'text'        => 'Gallery',
            'url'         => 'admin/gallery',
            'icon'        => 'file',
            'submenu' => [
                [
                    'text' => 'Gallery Images',
                    'url'  => 'admin/gallery',
                ],
                [
                    'text' => 'Add Gallary Videos',
                    'url'  => 'admin/gal_videos',
                ],
                [
                    'text' => 'Edit Gallary Videos',
                    'url'  => 'admin/gal_editvideos',
                ],
                [
                    'text' => 'Delete Gallary Videos',
                    'url'  => 'admin/gal_deletevideos',
                ]
            ]
        ],
        [
            'text'        => 'Contact Us',
            'url'         => 'admin/contact_us',
            'icon'        => 'file',
            'submenu' => [
                [
                    'text' => 'Add Contact Us Details',
                    'url'  => 'admin/contact_us',
                ],
                [
                    'text' => 'Edit Contact Details',
                    'url'  => 'admin/contact_list',
                ],
                [
                    'text' => 'Queries by users',
                    'url'  => 'admin/queries',
                ],
                // [
                //     'text' => 'Delete Contact Details',
                //     'url'  => 'admin/delcontact_list',
                // ]
            ]
            // 'label'       => 4,
            // 'label_color' => 'success',
        ],
        [
            'text'        => 'Google Map',
            'url'         => 'admin/map',
            'icon'        => 'file',
        ],
       
        'ACCOUNT',
        [
            'text' => 'Profile',
            'url'  => 'admin/settings',
            'icon' => 'user',
        ],
        // [
        //     'text' => 'Change Password',
        //     'url'  => 'admin/settings',
        //     'icon' => 'lock',
        // ],
        // [
        //     'text'    => 'Multilevel',
        //     'icon'    => 'share',
        //     'submenu' => [
        //         [
        //             'text' => 'Level One',
        //             'url'  => '#',
        //         ],
        //         [
        //             'text'    => 'Level One',
        //             'url'     => '#',
        //             'submenu' => [
        //                 [
        //                     'text' => 'Level Two',
        //                     'url'  => '#',
        //                 ],
        //                 [
        //                     'text'    => 'Level Two',
        //                     'url'     => '#',
        //                     'submenu' => [
        //                         [
        //                             'text' => 'Level Three',
        //                             'url'  => '#',
        //                         ],
        //                         [
        //                             'text' => 'Level Three',
        //                             'url'  => '#',
        //                         ],
        //                     ],
        //                 ],
        //             ],
        //         ],
        //         [
        //             'text' => 'Level One',
        //             'url'  => '#',
        //         ],
        //     ],
        // ],

        // 'LABELS',
        // [
        //     'text'       => 'Important',
        //     'icon_color' => 'red',
        // ],
        // [
        //     'text'       => 'Warning',
        //     'icon_color' => 'yellow',
        // ],
        // [
        //     'text'       => 'Information',
        //     'icon_color' => 'aqua',
        // ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Choose what filters you want to include for rendering the menu.
    | You can add your own filters to this array after you've created them.
    | You can comment out the GateFilter if you don't want to use Laravel's
    | built in Gate functionality
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Choose which JavaScript plugins should be included. At this moment,
    | only DataTables is supported as a plugin. Set the value to true
    | to include the JavaScript file from a CDN via a script tag.
    |
    */

    'plugins' => [
        'datatables' => true,
        'select2'    => true,
    ],
];
