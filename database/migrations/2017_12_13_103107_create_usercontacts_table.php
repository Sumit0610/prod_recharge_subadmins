<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsercontactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usercontacts', function (Blueprint $table) {
            $table->increments('id');
            $table->String('umail');
            $table->String('umobile');
            $table->String('uaddress');
            $table->String('facebook');
            $table->String('twitter');
            $table->String('linkedin');
            $table->String('instagram');
            $table->String('googleplus');
            $table->String('youtube');
             $table->integer('u_id')->unsigned();
             $table->foreign('u_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usercontacts');
    }
}
