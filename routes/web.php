<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'UserController@home');	

Route::get('admin/money_transfer', 'UserController@money_transfer');


Route::get('admin/about_add', 'UserController@about_add');
Route::post('admin/save_about_add', 'UserController@save_about_add');

Route::get('admin/update_about', 'UserController@update_about');
Route::post('admin/save_update_about', 'UserController@save_update_about');

Route::get('admin/service_add', 'UserController@service_add');
Route::post('admin/service_add', 'UserController@save_service_add');

Route::get('admin/service_update', 'UserController@service_update');
Route::post('admin/service_update', 'UserController@save_service_update');

Route::get('admin/edit_service_photo', 'UserController@edit_service_photo');
Route::post('admin/edit_service_photo', 'UserController@edit_service_photo_save');

Route::get('admin/service_delete', 'UserController@service_delete');
Route::post('admin/service_delete', 'UserController@save_service_delete');

Route::get('admin/view_contacts', 'UserController@view_contacts');

Route::post('admin/add_recipients', 'UserController@add_recipients');

Route::post('admin/send_money', 'UserController@send_money');

Route::get('admin/recharge_phone', 'UserController@recharge_phone');

Route::get('admin/new_old_user', 'UserController@new_old_user');

Route::post('admin/save_website_user', 'UserController@save_website_user');

Route::post('admin/verify_user', 'UserController@verify_user');

Route::get('admin/enter_otp', 'UserController@enter_otp');

Route::get('admin/add_user', 'UserController@add_user');

Route::get('admin/view_user', 'UserController@view_user');

Route::post('admin/save_subadmin_user', 'UserController@save_subadmin_user');

Route::post('/admin/send_mymoney', 'UserController@checkuserfirst');

Route::get('/admin/send_money_torecipient','UserController@view_userdata');

Route::get('/admin/sell_dth', 'UserController@sell_dth');

Route::post('/admin/save_sell_dth', 'UserController@save_sell_dth');

Route::get('admin/dthsales_history', 'UserController@dthsales_history');

Route::get('admin/pancard', 'UserController@pancard');

Route::get('admin/addsender', 'UserController@addsender');

Route::post('admin/addsender', 'UserController@savesender');


Route::get('admin/sender_otp', 'UserController@sender_otp');
Route::post('admin/sender_otp', 'UserController@verifysender');

Route::get('admin/add_recipient', 'UserController@add_recipient');

Route::post('admin/getamountdetails', 'UserController@getamountdetails');

Route::get('admin/dthsaleshist/{q}', 'UserController@dthsaleshist');

Route::get('admin/request_deposit', 'UserController@request_deposit');

Route::post('admin/save_request_deposit', 'UserController@save_request_deposit');

Route::get('admin/wallet_history', 'UserController@wallet_history');

Route::get('admin/get_pancard', 'UserController@get_pancard');

Route::post('admin/save_get_pancard', 'UserController@save_get_pancard');


Route::get('admin/view_pancardreport', 'UserController@view_pancardreport');

Route::get('admin/workinprogress', 'UserController@not_yetworked');

Route::get('admin/create_scheme', 'UserController@create_scheme');
Route::post('admin/create_scheme', 'UserController@save_scheme');
Route::get('admin/view_schemes', 'UserController@view_schemes');
Route::get('admin/updatescheme/{id}', 'UserController@updatescheme');
Route::post('admin/updatescheme', 'UserController@saveupdatedscheme');
Route::get('admin/deletescheme/{id}', 'UserController@deletescheme');