<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/admin/logo', 'HomeController@addlogo');
Route::post('/admin/logo', 'HomeController@logoadd');

Route::get('/admin/about_us', 'HomeController@aboutus');
Route::post('/admin/about_us', 'HomeController@aboutus_save');
Route::post('/admin/editteam', 'HomeController@aboutteam_save');
Route::post('/admin/about_desc', 'HomeController@aboutdesc_save');


Route::get('admin/about_list','HomeController@aboutlist');
Route::post('admin/about_list','HomeController@aboutlist_save');
Route::get('admin/delabout','HomeController@about_delete');
Route::post('admin/delaboutdesc','HomeController@aboutdesc_delete');
Route::post('admin/delabouttem','HomeController@abouttem_delete');

Route::get('/admin/services', 'HomeController@services');
Route::post('/admin/services', 'HomeController@addservices');
Route::get('/admin/service_list', 'HomeController@serviceslist');
Route::post('/admin/service_list', 'HomeController@service_list');
Route::get('/admin/delservice_list', 'HomeController@delservice_list');
Route::post('/admin/delservice', 'HomeController@delservice');


Route::get('/admin/gallery', 'HomeController@gallery');
Route::post('/admin/gallery', 'HomeController@savegallery');
Route::get('/admin/slider', 'HomeController@addslider');
Route::post('/admin/slider', 'HomeController@saveslider');
Route::get('/admin/gal_videos', 'HomeController@gal_videos');
Route::post('/admin/gal_videos', 'HomeController@savegal_videos');
Route::get('/admin/gal_editvideos', 'HomeController@gal_editvideos');
Route::post('/admin/gal_editvideos', 'HomeController@gal_savevideos');
Route::get('/admin/gal_deletevideos', 'HomeController@gal_deletevideos');
Route::post('/admin/gal_deletevideos', 'HomeController@gal_delvideos');


Route::get('/admin/map', 'HomeController@addmap');
Route::post('/admin/map', 'HomeController@savemap');

Route::get('/admin/template', 'HomeController@template');
Route::post('/admin/template', 'HomeController@save_template');

Route::get('/admin/settings', 'HomeController@profile_page');

Route::get('/admin/publish', 'HomeController@publish');
Route::post('/admin/publish', 'HomeController@save_publish');

Route::get('/admin/contact_us', 'HomeController@contact_us');
Route::post('/admin/contact_us', 'HomeController@save_contact_us');
Route::get('/admin/contact_list', 'HomeController@contact_list');
Route::post('/admin/contact_list', 'HomeController@update_contact_list');
Route::get('admin/delcontact_list', 'HomeController@del_contactlist');
Route::post('/admin/delconctmail', 'HomeController@delcontactmail');
Route::get('/admin/web_url', 'HomeController@weburl');
Route::post('/admin/web_url', 'HomeController@weburl_store');

Route::get('/{q}', 'HomeController@index_s');
Route::get('/admin/current_template', 'HomeController@current_template');

Route::get('/{w}/{t}/contact_us/{q}', 'HomeController@contactusdata');
Route::get('/{w}/{t}/about/{q}', 'HomeController@aboutdata');
Route::get('/{w}/{t}/gallery/{q}', 'HomeController@gallerydata');

Route::get('/admin/{w}/{d}', 'HomeController@demo_website');

Route::get('/admin/queries', 'HomeController@queries');

Route::get('/admin/add_branch', function(){
	return view('sskindia.add_branch');
});


Route::get('/state/getlist', 'HomeController@get_states');
Route::post('/city/getlist', 'HomeController@get_cities');

Route::get('/admin/add_vmission', 'HomeController@add_vmission');
Route::post('/admin/add_vmission', 'HomeController@save_vmission');

Route::get('/admin/vmission_list', 'HomeController@vmission_list');
Route::post('/admin/vmission_list', 'HomeController@save_vmission_list');

Route::get('/admin/delvm_list', 'HomeController@delvm_list');
Route::post('/admin/delvm_list', 'HomeController@delete_vmission_list');

Route::get('/admin/edit_branch', 'HomeController@edit_branch');
Route::get('/admin/delete_branch', 'HomeController@delete_branch');

Route::get('/admin/add_ourworks', 'HomeController@add_ourworks');
Route::post('/admin/save_works', 'HomeController@save_works');

Route::get('/admin/edit_ourworks', 'HomeController@edit_ourworks');
Route::post('/admin/edit_ourworks', 'HomeController@save_editworks');

Route::get('/admin/del_ourworks', 'HomeController@del_ourworks');
Route::post('/admin/del_ourworks', 'HomeController@delete_works');

Route::get('/admin/edit_photo', 'HomeController@edit_photo');

Route::post('/admin/send_mymoney', function(){
	echo "hiii";
});