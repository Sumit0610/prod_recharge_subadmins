@extends('adminlte::page')

@section('title', 'Admin LTE')

@section('content_header')

    <h1><b>Buy PAN-Card</b></h1>
@stop    

@section('wallet_balance')
        <a href="{{ url('admin/request_deposit') }}"><p style="float: right; font-size: 15px; margin-top: 10px; margin-bottom: 10px; margin-left: 10px;"><b>Request Deposit</b></p></a>

        <p style="float: right; font-size: 15px; margin-bottom: 10px; margin-top: 10px;"><b>Balance: ₹{{$userdata->wallet_balance}}</b></p>

@stop

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style type="text/css"><style>
.card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 50%;
}

.card:hover {
    box-shadow: 0 0 50px 0 rgba(0,0,0,0.2);
}

.container {
    padding: 2px 16px;
    padding-top: 20px;
    padding-bottom: 0px;
}
</style></style>


@section('content')

	<div class="container">
     <div class="box box-primary" style="padding: bottom: 0px;">
<!--           <h2> &nbsp; &nbsp;ABOUT US</h2>
 -->

            @if(session('status'))
                <div class="alert alert-success">
                {{ session('status') }}
                </div>
            @endif

            @if(session('error'))
                <div class="alert alert-danger">
                {{ session('error') }}
                </div>
            @endif                

                        <div class="row box-body">
                            <div class="col-lg-10">

                                    @if(($userdata->wallet_balance - 112) >= 0)
                                        <form id="register-form" action="{{ URL('admin/save_get_pancard') }}" method="post" role="form">

                                        <div class="form-group col-md-8">
                                            <input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="User Name" required>
                                        </div>
                                        <div class="form-group col-md-8">
                                            <input type="number" name="mobile_no" id="mobile_no" tabindex="1" class="form-control" placeholder="User Mobile" required>
                                        </div>
                                        <div class="form-group col-md-8">
                                            <input type="text" name="city" id="city" tabindex="1" class="form-control" placeholder="City" required>
                                        </div>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="userid" value="{{ $userdata->id }}">
                                        
                                        <div class="form-group col-md-8">
                                        <label> Rs. 112 will be deducted from your wallet! Current balance: Rs. {{$userdata->wallet_balance}}</label>
                                        <div class="form-group float-label-control">
                                        <input type="submit" name="register-submit" class="form-control" value="Save details" style="background: #00B9F5; color: white;">
                                        </div></div>

                                        </form>
                                     @else
                                        <p>Kindly credit your wallet with some amount to use this feature! Current balance: Rs. {{$userdata->wallet_balance}}. <a href="{{URL('admin/request_deposit')}}">Request deposit!</a></p>   
                                     @endif
                                
                            </div>
                        </div>
          
        </div></div>
   

@stop