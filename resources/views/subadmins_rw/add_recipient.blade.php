@extends('adminlte::page')

@section('title', 'APG Online Services')

@section('content_header')
    <h1><b>Add Benificiary</b></h1>
@stop    

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style type="text/css"><style>
.card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 50%;
}

.card:hover {
    box-shadow: 0 0 50px 0 rgba(0,0,0,0.2);
}

.container {
    padding: 2px 16px;
    padding-top: 20px;
    padding-bottom: 0px;
}
</style></style>


@section('content')

	<div class="container">
     <div class="box box-primary" style="padding: bottom: 0px;">
<!--           <h2> &nbsp; &nbsp;ABOUT US</h2>
 -->

            @if(session('status'))
                <div class="alert alert-success">
                {{ session('status') }}
                </div>
            @endif

            @if(session('error'))
                <div class="alert alert-danger">
                {{ session('error') }}
                </div>
            @endif                

                        <div class="row box-body">
                            <div class="col-lg-10">
                                    <form method="POST" action="{{ URL('admin/add_recipients') }}"> 
                                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                    <div class="form-group float-label-control">
                                    <label for="">Sender Mobile Number</label>
                                    <input type="text" readonly id="sender_mobile" name="sender_mobile" class="form-control" placeholder="Sender Mobile Number" value="{{$mobile}}">
                                    </div>
                                    <div class="form-group float-label-control">
                                    <label for="">Benificiary Bank Account Number</label>
                                    <input type="text" id="account_number" name="account_number" class="form-control" placeholder="Enter Benificiary Bank Account Number">
                                    </div>
                                    <div class="form-group float-label-control">
                                    <label for="">Benificiary Bank IFSC Code</label>
                                    <input type="text" id="ifsc_code" name="ifsc_code" class="form-control" placeholder="Enter Benificiary Bank IFSC Code">
                                    </div>
                                    <div class="form-group float-label-control">
                                    <label for="">Benificiary Name</label>
                                    <input type="text" id="rec_name" name="rec_name" class="form-control" placeholder="Enter Benificiary Name">
                                    </div>
                                    <div class="form-group float-label-control">
                                    <label for="">Benificiary Contact</label>
                                    <input type="text" id="rec_mobile" name="rec_mobile" class="form-control" placeholder="Enter Benificiary Contact">
                                    </div>
                                    <div class="form-group float-label-control">
                                    <input type="submit" class="form-control" value="Add Benificiary" style="background: #00B9F5; color: white;">
                                    </div>
                                </form>
                                
                            </div>
                        </div>
          
        </div></div>
    
    

      

@stop