@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><b>PAN Card</b></h1>
@stop

@section('wallet_balance')
           <a href="{{ url('admin/request_deposit') }}"><p style="float: right; font-size: 15px; margin-top: 10px; margin-bottom: 10px; margin-left: 10px;"><b>Request Deposit</b></p></a>

        <p style="float: right; font-size: 15px; margin-bottom: 10px; margin-top: 10px;"><b>Balance: ₹{{$userdata->wallet_balance}}</b></p>

@stop

@section('content')

<style>
.button {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 5px 5px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 15px;
    margin: auto;
    -webkit-transition-duration: 0.4s; /* Safari */
    transition-duration: 0.4s;
    cursor: pointer;
    top: 50%;
}

.wrapperss {
    text-align: center;
    background: white;
    margin-bottom: 30px;
}

.button1 {
    background-color: white; 
    color: black; 
    border: 2px solid #4CAF50;
}

.button1:hover {
    background-color: #4CAF50;
    color: white;
}

#documents_needed {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#documents_needed td, #documents_needed th {
    border: 1px solid #ddd;
    padding: 4px;
    font-size: 12px;
}

#documents_needed tr:nth-child(even){background-color: #f2f2f2;}

#documents_needed tr:hover {background-color: #ddd;}

#documents_needed th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #00A65A;
    color: white;
}

</style>

  <div class="container">
     <div class="box box-primary">

      <div class="box-body">
        
      @foreach ($errors->all() as $error)
        <p class="alert alert-danger">{{ $error }}</p>
      @endforeach
      @if(session('status'))
        <div class="alert alert-success">
        {{ session('status') }}
        </div>
      @endif

        <div>

          <div class="col-md-4"><div class="wrapperss"><a href="http://23.92.20.71/documents/Form49A.pdf" target="_blank"><button class="button button1">New PAN Card</button></a></div>
          </div>
          <div class="col-md-4"><div class="wrapperss"><a href="{{ URL('admin/get_pancard') }}"><button class="button button1">Buy PAN Card</button></a></div>
          </div>
          <div class="col-md-4"><div class="wrapperss"><a href="http://23.92.20.71/documents/CSF.pdf" target="_blank"><button class="button button1">Lost PAN Card</button></a></div>
          </div>
          </div>

        </div>

        <div style="width: 90%;"><p class="text-center" style="font-size: 15px;"><b>List of documents</b></p></div>

        <div>

          <div> <div style="width: 90%; margin-left: auto; margin-right: auto; margin-bottom: 10px;">
        <table id="documents_needed">
            <tr>
                <th width="35%">
                    PHOTO ID PROOF
                </th>
                <th width="35%">
                    ADDRESS PROOF
                </th>
                <th width="30%">
                    BIRTH DATE PROOF
                </th>
            </tr>
            <tr class="Grey8pt">
                <td width="35%">
                    Driving Licence
                </td>
                <td td width="35%">
                    Driving Licence
                </td>
                <td td width="30%">
                    Driving Licence
                </td>
            </tr>
            <tr>
                <td>
                    Passport
                </td>
                <td>
                    Passport
                </td>
                <td>
                    Passport
                </td>
            </tr>
            <tr>
                <td>
                    Adhar Card
                </td>
                <td>
                    Adhar Card
                </td>
                <td>
                    Birth Certificate
                </td>
            </tr>
            <tr>
                <td>
                    Voting Card
                </td>
                <td>
                    Voting Card
                </td>
                <td>
                    Domicile certificate issued by Govt
                </td>
            </tr>
            <tr>
                <td>
                    Ration Card with Photo
                </td>
                <td>
                    Photo certificate annexure A (MP MLA letter) in original
                </td>
                <td>
                    SSC Passing certificate
                </td>
            </tr>
            <tr>
                <td>
                    Lhoto Icard issued by Central/State/Public sector undertaking
                </td>
                <td>
                    Latest Electricity Bill
                </td>
                <td>
                    HSC Passing certificate
                </td>
            </tr>
            <tr>
                <td>
                    Pensioner/CGHS card
                </td>
                <td>
                    Latest Water Bill
                </td>
                <td>
                    Affidavit sworn before magistrate stating DoB
                </td>
            </tr>
            <tr>
                <td>
                    Arms license
                </td>
                <td>
                    Latest Landline telephone bill
                </td>
                <td>
                    Pension Payment Order
                </td>
            </tr>
            <tr>
                <td>
                    Photo certificate annexure A ( MP MLA letter ) in original
                </td>
                <td>
                    Latest Bank statement
                </td>
                <td>
                    Marriage certificate issued by Registrar of Marriages with DOB
                </td>
            </tr>
            <tr>
                <td>
                    Photo Bank certificate (Annexure C ) in original
                </td>
                <td>
                    Latest Depository statement
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    Latest Credit card statement
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    Consumer gas connection card
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    Post office passbook
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    Domicile certificate issued by Central Govt.
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    Property tax assessment order
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    Allotment letter of accommodation issued by the Central Government or State Government
                    of not more than three years old
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    Property registration certificate
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    Employer certificate ( Annexure B)
                </td>
                <td>
                </td>
            </tr>
        </table>
    </div>
    <div id="ContentPlaceHolder1_divVKV" style="width: 90%; margin-left: auto; margin-right: auto; margin-bottom: 10px;">
        <div class="panImpTxt">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <strong>1</strong>. Take Printout Of The Form
                    </td>
                    <td>
                        <strong>2</strong>. Fill The Form And Attach Required Documents
                    </td>
                    <td>
                        <strong>3</strong>. Courier The Documents To
                    </td>
                </tr>
            </table>
<br>
            <p class="text-center" style="background: #00A65A; color: white; padding: 10px;"><b>Address: Nagpur, Maharashtra </b></p><br>
        </div>
</div>
          </div>
          

        </div>
                    
      </div>

     </div>
  </div>

@stop