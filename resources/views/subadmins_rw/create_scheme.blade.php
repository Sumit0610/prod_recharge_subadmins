@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><b>Create Scheme</b></h1>
@stop

@section('content')

<style>
.card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 100%;
}

.card:hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

.container {
    padding: 2px 16px;
    padding-top: 20px;
}
</style>

  <div class="container">
     <div class="box box-primary">
<!--           <h2> &nbsp; &nbsp; ServiceList</h2>
 -->

      <div class="box-body">
        
      @foreach ($errors->all() as $error)
        <p class="alert alert-danger">{{ $error }}</p>
      @endforeach
      @if(session('status'))
        <div class="alert alert-success">
        {{ session('status') }}
        </div>
      @endif

      @if(session('error'))
        <div class="alert alert-danger">
        {{ session('error') }}
        </div>
      @endif
          
    
    <form action="{{ url('/admin/create_scheme') }}" enctype="multipart/form-data" method="post">

        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <input type="hidden" name="commisionid" value="">
        
             
             <div class="col-md-12">
                <div class="form-group col-md-8">
                   <label for="schemename">Scheme Name</label>
                   <input type="text" name="schemename" id="schemename" class="form-control col-md-12" value="" required>
                </div>
             </div>

             <div class="col-md-12">
                <div class="form-group col-md-8">
                   <label for="rechargecommission">Recharge Commission(In %)</label>
                  <input type="text" name="rechargecommission" id="rechargecommission" class="form-control col-md-12" value="" required>
                </div>
             </div>

             <!-- <div class="col-md-12">
                <div class="form-group col-md-8">
                   <label for="dthcommission">DTH Commission(In %)</label>
                  <input type="text" name="dthcommission" id="dthcommission" class="form-control col-md-12" value="" required>
                </div>
             </div> -->

             <div class="col-md-12">
                <div class="form-group col-md-8">
                  <label for="moneytransfercommission">Money Transfer Commission(In %)</label>
                  <input type="text" name="moneytransfercommission" id="moneytransfercommission" class="form-control col-md-12" value="" required>
                </div>
             </div>

             <div class="col-md-12">
                <div class="form-group col-md-8">
                  <label for="pancardcommission">Pan card Commission(In %)</label>
                  <input type="text" name="pancardcommission" id="pancardcommission" class="form-control col-md-12" value="" required>
                </div>
             </div>

             <br>
             <div class="col-md-12">
                <div class="form-group">
                  <button type="submit" class="btn btn-info" name="sub1"><i class="fa fa-save"></i>  Create Scheme</button>
                </div>  
             </div>
  
          </form>
            

      </div>

      

    

     </div>
    </div>

@stop