@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><b>Scheme List</b></h1>
@stop

@section('content')

<style>
.card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 100%;
}

.card:hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

.container {
    padding: 2px 16px;
    padding-top: 20px;
}
</style>

  <div class="container">
     <div class="box box-primary">
<!--           <h2> &nbsp; &nbsp; ServiceList</h2>
 -->

      <div class="box-body">
        
      @foreach ($errors->all() as $error)
        <p class="alert alert-danger">{{ $error }}</p>
      @endforeach
      @if(session('status'))
        <div class="alert alert-success">
        {{ session('status') }}
        </div>
      @endif

      @if(session('error'))
        <div class="alert alert-danger">
        {{ session('error') }}
        </div>
      @endif
          
    
    <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Index</th>
                    <th>Scheme name</th>
                    <!-- <th>DTH Commission</th> -->
                    <th>Recharge commission</th>
                    <th>Pancard commission</th>
                    <th>Money Transfer Commission</th>
                    <th>Update Scheme</th>
                    <!-- <th>Delete Scheme</th> -->
                  </tr>
                </thead>
                <tbody>
                  
                    @foreach($scheme as $sch)
                      <tr>
                      <td>{{$i++}}</td>
                      <td>{{$sch->scheme_name}}</td>
                      <!-- <td>{{$sch->dthsales_surcharge}}</td> -->
                      <td>{{$sch->recharge_surcharge}}</td>
                      <td>{{$sch->pancard_surcharge}}</td>
                      <td>{{$sch->moneytransf_surcharge}}</td>
                      <td><a href="{{URL('admin/updatescheme',['id'=>$sch->id])}}" class="btn btn-primary btn-sm">Update Scheme</a></td>
                      <!-- <td><a href="{{URL('admin/deletescheme',['id'=>$sch->id])}}" class="btn btn-primary btn-sm" id="deletescheme" onclick="return confirm('Are you sure?')">Delete Scheme</a></td>
                      </tr> -->
                    @endforeach
                  
                </tbody>
             </table>
            

      </div>

      

    

     </div>
    </div>

@stop