@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><b>Request Deposit</b></h1>
@stop

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

@section('wallet_balance')
           <a href="{{ url('admin/request_deposit') }}"><p style="float: right; font-size: 15px; margin-top: 10px; margin-bottom: 10px; margin-left: 10px;"><b>Request Deposit</b></p></a>

        <p style="float: right; font-size: 15px; margin-bottom: 10px; margin-top: 10px;"><b>Balance: ₹{{$userdata->wallet_balance}}</b></p>

@stop

@section('content')

<style>
.button {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 5px 5px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 15px;
    margin: auto;
    -webkit-transition-duration: 0.4s; /* Safari */
    transition-duration: 0.4s;
    cursor: pointer;
    top: 50%;
}

.wrapperss {
    text-align: center;
    background: white;
    margin-bottom: 30px;
}

.button1 {
    background-color: white; 
    color: black; 
    border: 2px solid #4CAF50;
}

.button1:hover {
    background-color: #4CAF50;
    color: white;
}

#documents_needed {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#documents_needed td, #documents_needed th {
    border: 1px solid #ddd;
    padding: 4px;
    font-size: 12px;
}

#documents_needed tr:nth-child(even){background-color: #f2f2f2;}

#documents_needed tr:hover {background-color: #ddd;}

#documents_needed th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #00A65A;
    color: white;
}

</style>

  <div class="container">
     <div class="box box-primary">

      <div class="box-body">
        
      @foreach ($errors->all() as $error)
        <p class="alert alert-danger">{{ $error }}</p>
      @endforeach
      @if(session('status'))
        <div class="alert alert-success">
        {{ session('status') }}
        </div>
      @endif


            <div class="row box-body">
                            <div class="col-lg-10">
                                    <form id="register-form" action="{{ URL('admin/save_request_deposit') }}" method="post" role="form">

                                    <input id="make_text" type = "hidden" name = "make_text" value = "" />

                                    <div class="form-group col-md-8">
                                        <label for="agentname"> Agent Name </label>
                                        <input type="text" name="agentname" id="agentname" tabindex="1" class="form-control" placeholder="Agent Name" value="{{ $userdata->name }}" readonly>
                                    </div>
                                    <div class="form-group col-md-8">
                                        <label for="agentcontact"> Agent Contact </label>
                                        <input type="text" name="agentcontact" id="agentcontact" tabindex="1" class="form-control" placeholder="Agent Contact" value="{{ $userdata->user_phone }}" readonly>
                                    </div>
                                     <div class="form-group col-md-8">
                                        <label for="amount"> Amount </label>
                                        <input type="number" name="amount" id="amount" tabindex="1" class="form-control">
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="payment_date"> Date of deposit </label>
                                        <input type="date" name="payment_date" id="payment_date" tabindex="1" class="form-control">
                                    </div>
                                    <div class="form-group col-md-8">
                                    <label for="deposit_type">Deposit Type</label>
                                      <select name="deposit_type" id="deposit_type" class="col-md-12">
                                        <option value="NA" selected>-------Select operator-------</option>

                                        <option value="0">Cheque</option>
                                        <option value="1">Cash</option>
                                        <option value="2">NEFT/RTGS</option>
                                        <option value="3">ATM Transfer</option>
                                        <option value="4">Credit</option>
                                        <option value="5">Refund</option>
                                      </select>
                                    </div> 

                                    <div id="payment_cheque" style="display: none; margin-left: 10px;">     

                                    <div class="form-group col-md-8" id="divcheque">
                                        <label for="issuance_date"> Cheque/DD No. </label>
                                       <input type="text" name="cheque_no" id="cheque_no" tabindex="1" class="form-control" placeholder="Cheque/DD No" value="" >
                                    </div>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <div class="form-group col-md-8" id="divbankacc">
                                        <label for="issuance_date"> Your Bank Account No </label>
                                        <input type="text" name="bankaccno" id="bankaccno" tabindex="1" class="form-control" placeholder="Your Bank Account No" value="" >
                                    </div>

                                    <div class="form-group col-md-8" id="divbankname">
                                         <label for="issuance_date"> Your Bank Name </label>
                                       <input type="text" name="bankname" id="bankname" tabindex="1" class="form-control" placeholder="Your Bank Name" value="" >
                                    </div>

                                    <div class="form-group col-md-8" id="divbankbranch">
                                       <label for="issuance_date"> Your bank branch </label>
                                        <input type="text" name="bankbranch" id="bankbranch" tabindex="1" class="form-control" placeholder="Your Bank Branch Name" value="" >
                                    </div>

                                   <div class="form-group col-md-5">
                                        <label for="issuance_date"> Date of issuance </label>
                                        <input type="date" name="issuance_date" id="issuance_date" tabindex="1" class="form-control">
                                    </div>

                                    </div>


                                    <div id="payment_cash" style="display: none; margin-left: 10px;">     

                                    <div class="form-group col-md-8">
                                        <label for="issuance_date"> Benificiary Bank </label>
                                       <input type="text" name="benificiary_bank" id="benificiary_bank" tabindex="1" class="form-control" placeholder="Benificiary Bank" value="" >
                                    </div>

                                    <div class="form-group col-md-8">
                                        <label for="issuance_date"> Benificiary Bank Branch </label>
                                        <input type="text" name="benificiary_bankbranch" id="benificiary_bankbranch" tabindex="1" class="form-control" placeholder="Benificiary Bank Branch" value="" >
                                    </div>

                                    </div>


                                    <div id="payment_neft_rtgs" style="display: none; margin-left: 10px;">     

                                    <div class="form-group col-md-8">
                                        <label for="issuance_date"> Benificiary Bank Name </label>
                                       <input type="text" name="benificiary_bank_name" id="benificiary_bank_name" tabindex="1" class="form-control" placeholder="Benificiary Bank Name" value="" >
                                    </div>

                                    <div class="form-group col-md-8">
                                        <label for="issuance_date"> Your Bank Name </label>
                                        <input type="text" name="bank_name" id="bank_name" tabindex="1" class="form-control" placeholder="Your Bank Name" value="" >
                                    </div>

                                    <div class="form-group col-md-8">
                                        <label for="issuance_date"> Account Holder Name </label>
                                        <input type="text" name="accountholder_name" id="accountholder_name" tabindex="1" class="form-control" placeholder="Account Holder Name" value="" >
                                    </div>

                                    <div class="form-group col-md-8">
                                        <label for="issuance_date"> Transaction ID </label>
                                        <input type="text" name="transaction_id" id="transaction_id" tabindex="1" class="form-control" placeholder="Transaction ID" value="" >
                                    </div>

                                    </div>


                                    <div id="payment_atm_transfer" style="display: none; margin-left: 10px;">     

                                    <div class="form-group col-md-8">
                                        <label for="issuance_date"> Benificiary Bank Name </label>
                                       <input type="text" name="atmbenificiary_bank_name" id="atmbenificiary_bank_name" tabindex="1" class="form-control" placeholder="Benificiary Bank Name" value="" >
                                    </div>

                                    <div class="form-group col-md-8">
                                        <label for="issuance_date"> Transaction ID </label>
                                        <input type="text" name="atmtransaction_id" id="atmtransaction_id" tabindex="1" class="form-control" placeholder="Transaction ID" value="" >
                                    </div>

                                    </div>


                                    <div id="payment_credit" style="display: none; margin-left: 10px;">     

                                    <div class="form-group col-md-8">
                                        <label for="issuance_date"> Reference Name </label>
                                       <input type="text" name="reference_name" id="reference_name" tabindex="1" class="form-control" placeholder="Reference Name" value="" >
                                    </div>

                                    <div class="form-group col-md-8">
                                        <label for="issuance_date"> Transaction ID </label>
                                        <input type="text" name="credittransaction_id" id="credittransaction_id" tabindex="1" class="form-control" placeholder="Transaction ID" value="" >
                                    </div>

                                    </div>

                                    <div class="form-group col-md-8">
                                        <label for="remarks"> Remarks </label>
                                        <input type="text" name="remarks" id="remarks" tabindex="1" class="form-control" placeholder="Remarks" value="">
                                    </div>
                                    
                                    <div class="form-group col-md-8">
                                    <div class="form-group float-label-control">
                                    <input type="submit" name="register-submit" class="form-control" value="Send Request" style="background: #00B9F5; color: white;">
                                    </div></div>
                                </form>
                                
                            </div>
                        </div>

                    
      </div>

     </div>
  </div>

  <script type="text/javascript">

    $('select').on('change', function() {

                var thisval = this.value;

                $('#payment_cheque').hide();
                $('#payment_cash').hide();
                $('#payment_neft_rtgs').hide();
                $('#payment_atm_transfer').hide();
                $('#payment_credit').hide();

   
                if(this.value == "NA") {
                   
                }
                else if(this.value == "0") {    
                    $('#payment_cheque').show();
                }
                else if(this.value == "1") {   
                    $('#payment_cash').show();                
                }
                else if(this.value == "2") {                   
                    $('#payment_neft_rtgs').show();                
                }
                else if(this.value == "3") {                   
                    $('#payment_atm_transfer').show();                
                }
                else if(this.value == "4") {                   
                    $('#payment_credit').show();                
                }

                else {                   
                }
            })


  </script>

@stop