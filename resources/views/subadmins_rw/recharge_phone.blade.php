@extends('adminlte::page')

@section('title', 'APG Online Services')

@section('content_header')
    <h1><b>Recharge</b></h1>
@stop    

@section('wallet_balance')
           <a href="{{ url('admin/request_deposit') }}"><p style="float: right; font-size: 15px; margin-top: 10px; margin-bottom: 10px; margin-left: 10px;"><b>Request Deposit</b></p></a>

        <p style="float: right; font-size: 15px; margin-bottom: 10px; margin-top: 10px;"><b>Balance: ₹{{$userdata->wallet_balance}}</b></p>

@stop

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style type="text/css"><style>
.card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 50%;
}

.card:hover {
    box-shadow: 0 0 50px 0 rgba(0,0,0,0.2);
}

.container {
    padding: 2px 16px;
    padding-top: 20px;
    padding-bottom: 0px;
}

.white {
    background-color: white;
}

</style></style>


@section('content')


	<div class="container">
     <div class="box box-primary" style="padding: bottom: 0px;">
<!--           <h2> &nbsp; &nbsp;ABOUT US</h2>
 -->

        @if($user_verified == 1)
          
		  	@foreach ($errors->all() as $error)
				<p class="alert alert-danger">{{ $error }}</p>
			@endforeach
			@if(session('status'))
				<div class="alert alert-success">
				{{ session('status') }}
				</div>
			@endif
            @if(session('error'))
                <div class="alert alert-danger">
                {{ session('error') }}
                </div>
            @endif

            <div class="col-md-8 box-body" style="margin-top: 20px; ">

                <div class="col-md-3 col-sm-3 col-lg-3 col-xs-3" style="margin-right: 0px; padding-right: 1px;">
                    <button id="add_recipients" class="small-box-footer" style="background: #e1e1e1; width: 100%; height: 45px;">Mobile</button>
                </div>

                <div class="col-md-3 col-sm-3 col-lg-3 col-xs-3" style="margin-left: 0px; padding-left: 1px; padding-right: 1px;">                
                    <button id="view_recipients" class="small-box-footer" style="background: white; width: 100%; height: 45px; ">DTH</button>
                </div>

                <div class="col-md-3 col-sm-3 col-lg-3 col-xs-3" style="margin-left: 0px; padding-left: 1px; padding-right: 1px;">                
                    <button id="postpaid_view" class="small-box-footer" style="background: white; width: 100%; height: 45px; ">Postpaid</button>
                </div>

                <div class="col-md-3 col-sm-3 col-lg-3 col-xs-3" style="margin-left: 0px; padding-left: 1px;">                
                    <button id="datacard_view" class="small-box-footer" style="background: white; width: 100%; height: 45px;">Datacard</button>
                </div>


<!--                 <div class="col-lg-3 col-sm-6">
                <div class="small-box bg-aqua">
                <div class="inner">
                <h4><b>MSEB Bills</b></h4>

                <p>Bill payment</p>
                </div>
                <div class="icon" style="margin-top: 10px;">
                <i class="fa fa-paper-plane"></i>
                </div>
                <button id="send_money" class="small-box-footer" style="background: #1D9D73; width: 100%;">Click to pay bills &nbsp;<i class="fa fa-arrow-circle-right"></i></button>
                </div>
                </div> -->

            </div>
 				

            <div id="sendmoney"  class="wow fadeInUp animated box-body" data-wow-delay=".3s" data-wow-duration="500ms"> 

                
            <form method="POST" action="http://23.92.20.71/PHPFiles/rechargeapi1.php"> 
                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="userid" id="userid" value="{{ $ids }}">

                <div class="form-group float-label-control col-md-8">
                <label for="">Mobile Number</label>
                <input type="number" id="rec_mobile" name="rec_mobile" class="form-control" placeholder="Enter mobile number">
                </div>
                 <div class="form-group float-label-control col-md-8">
                <label for="">Amount</label>
                <input type="number" id="amount" name="amount" class="form-control" placeholder="Enter Amount">
                </div>
                <div class="form-group float-label-control col-md-8">
                <label for="">Operator</label>
                <select name="operator" id="operator">
                    <option value="AC">Aircel</option>
                    <option value="AT">Airtel</option>
                    
                    <option value="BT">BSNL (Topup)</option>
                    <option value="BV">BSNL (Validity)</option>
                    <option value="ID">Idea</option>
                    
                    <option value="RC">Reliance CDMA</option>
                    <option value="RG">Reliance GSM</option>
                   
                    <option value="TS">TATA Docomo (SP)</option>
                    <option value="TN">TATA Docomo (Normal)</option>
                    <option value="TI">Tata Indicom</option>
                    <option value="UN">Uninor</option>
                    <option value="US">Uninor (SP)</option>
                    <option value="VI">Virgin/T24</option>
                    <option value="VS">Virgin/T24(SP)</option>
                    <option value="RV">Vodafone</option>
                </select>
                </div>
                <div class="form-group float-label-control col-md-8">
                <input type="submit" class="form-control" value="Recharge" style="background: #00B9F5; color: white;">
                </div>
            </form>

             
            </div>


        <div id="postpaid_div"  class="wow fadeInUp animated box-body" data-wow-delay=".3s" data-wow-duration="500ms" style="display:none;"> 

                
            <form method="POST" action="http://23.92.20.71/PHPFiles/rechargeapi1.php"> 
                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="userid" id="userid" value="{{ $ids }}">

                <div class="form-group float-label-control col-md-8">
                <label for="">Mobile Number</label>
                <input type="number" id="rec_mobile" name="rec_mobile" class="form-control" placeholder="Enter mobile number">
                </div>
                 <div class="form-group float-label-control col-md-8">
                <label for="">Amount</label>
                <input type="number" id="amount" name="amount" class="form-control" placeholder="Enter Amount">
                </div>
                <div class="form-group float-label-control col-md-8">
                <label for="">Operator</label>
                <select name="operator" id="operator">
                
                    <option value="PA">Airtel(Postpaid)</option>
                    <option value="PB">BSNL (Postpaid)</option>                
                    <option value="PI">Idea (Postpaid)</option>
                    <option value="PR">Reliance (Postpaid)</option>
                
                    <option value="PD">TATA Docomo (Postpaid)</option>
                
                    <option value="PV">Vodafone (Postpaid)</option>
                </select>
                </div>
                <div class="form-group float-label-control col-md-8">
                <input type="submit" class="form-control" value="Recharge" style="background: #00B9F5; color: white;">
                </div>
            </form>

             
            </div>


            <div id="default_hide_viewrecipients"  class="wow fadeInUp animated box-body" data-wow-delay=".3s" style="display:none;" data-wow-duration="500ms"> 

                
            <form method="POST" action="http://23.92.20.71/PHPFiles/rechargeapi1.php"> 
                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="userid" id="userid" value="{{ $ids }}">

                <div class="form-group float-label-control col-md-8">
                <label for="">Mobile Number</label>
                <input type="number" id="rec_mobile" name="rec_mobile" class="form-control" placeholder="Enter mobile number">
                </div>
                 <div class="form-group float-label-control col-md-8">
                <label for="">Amount</label>
                <input type="number" id="amount" name="amount" class="form-control" placeholder="Enter Amount">
                </div>
                <div class="form-group float-label-control col-md-8">
                <label for="">Operator</label>
                <select name="operator" id="operator">
                    <option value="DA">Airtel Digital TV</option>
                    <option value="DD">Dish TV</option>
                    <option value="DB">Reliance Big TV</option>
                </select>
                </div>
                <div class="form-group float-label-control col-md-8">
                <input type="submit" class="form-control" value="Recharge" style="background: #00B9F5; color: white;">
                </div>
            </form>

             
            </div>


            <div id="datacard_div"  class="wow fadeInUp animated box-body" data-wow-delay=".3s" style="display:none;" data-wow-duration="500ms"> 
             
            </div>


            <div id="default_hide_addrecipients"  class="wow fadeInUp animated box-body" data-wow-delay=".3s" data-wow-duration="500ms" style="display:none;"> 

                
                <form method="POST" action="http://23.92.20.71/PHPFiles/rechargeapi1.php"> 
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="userid" id="userid" value="{{ $ids }}">
                    <div class="form-group float-label-control col-md-8">
                    <label for="">Mobile Number</label>
                    <input type="number" id="rec_mobile" name="rec_mobile" class="form-control" placeholder="Enter mobile number">
                    </div>
                     <div class="form-group float-label-control col-md-8">
                    <label for="">Amount</label>
                    <input type="number" id="amount" name="amount" class="form-control" placeholder="Enter Amount">
                    </div>
                    <div class="form-group float-label-control col-md-8">
                    <label for="">Operator</label>
                    <select name="operator" id="operator">
                        <option value="AC">Aircel</option>
                        <option value="AT">Airtel</option>
                        <option value="BT">BSNL (Topup)</option>
                        <option value="BV">BSNL (Validity)</option>
                        <option value="ID">Idea</option>
                        <option value="RC">Reliance CDMA</option>
                        <option value="RG">Reliance GSM</option>
                        <option value="TS">TATA Docomo (SP)</option>
                        <option value="TN">TATA Docomo (Normal)</option>
                        <option value="TI">Tata Indicom</option>
                        <option value="UN">Uninor</option>
                        <option value="US">Uninor (SP)</option>
                        <option value="VI">Virgin/T24</option>
                        <option value="VS">Virgin/T24(SP)</option>
                        <option value="RV">Vodafone</option>
                    </select>
                    </div>
                    <div class="form-group float-label-control col-md-8">
                    <input type="submit" class="form-control" value="Recharge" style="background: #00B9F5; color: white;">
                    </div>
                </form>

             
            </div>

       @else 
       

        <h3 style="padding-bottom: 4px;">Please contact APG Online Services and get your account verified first!</h3>     

		   
       @endif     

     </div>
    </div>
        
    <script type="text/javascript">
        
        $(document).ready(function(){


            $("#add_recipients").click(function(){
                $("#default_hide_addrecipients").show();
                $("#default_hide_viewrecipients").hide();
                $("#view_recipients").css("background-color", "white");
                $("#add_recipients").css("background-color", "#e1e1e1");
                $("#sendmoney").hide();
                  $("#postpaid_div").hide();
         $("#postpaid_view").css("background-color", "white");
          $("#datacard_div").hide();
         $("#datacard_view").css("background-color", "white");
            });

            $("#view_recipients").click(function(){
                $("#default_hide_addrecipients").hide();
                $("#default_hide_viewrecipients").show();
                $("#add_recipients").addClass("white");
                $("#view_recipients").css("background-color", "#e1e1e1");
                $("#add_recipients").css("background-color", "white");
                $("#sendmoney").hide();
                  $("#postpaid_div").hide();
         $("#postpaid_view").css("background-color", "white");
          $("#datacard_div").hide();
         $("#datacard_view").css("background-color", "white");
            });

            $("#postpaid_view").click(function(){
                $("#default_hide_addrecipients").hide();
                $("#default_hide_viewrecipients").hide();
                $("#add_recipients").addClass("white");
                $("#view_recipients").css("background-color", "white");
                $("#add_recipients").css("background-color", "white");
                $("#postpaid_view").css("background-color", "#e1e1e1");
                $("#sendmoney").hide();
                $("#postpaid_div").show();  
                $("#datacard_div").hide();
                $("#datacard_view").css("background-color", "white");
            });
 
            $("#datacard_view").click(function() {
                $("#default_hide_addrecipients").hide();
                $("#default_hide_viewrecipients").hide();
                $("#add_recipients").addClass("white");
                $("#view_recipients").css("background-color", "white");
                $("#add_recipients").css("background-color", "white");
                $("#datacard_view").css("background-color", "#e1e1e1");
                 $("#postpaid_div").hide();
                 $("#postpaid_view").css("background-color", "white");
                $("#sendmoney").hide();
                $("#datacard_div").show();
            })

        }); 

    </script>    
    

      

@stop