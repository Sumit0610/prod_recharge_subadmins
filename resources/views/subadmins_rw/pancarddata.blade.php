@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><b>PAN Card Sales History</b></h1>
@stop

@section('wallet_balance')
           <a href="{{ url('admin/request_deposit') }}"><p style="float: right; font-size: 15px; margin-top: 10px; margin-bottom: 10px; margin-left: 10px;"><b>Request Deposit</b></p></a>

        <p style="float: right; font-size: 15px; margin-bottom: 10px; margin-top: 10px;"><b>Balance: ₹{{$userdata->wallet_balance}}</b></p>

@stop

@section('content')

<style>
.card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 100%;
}

.card:hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

.container {
    padding: 2px 16px;
    padding-top: 20px;
}
</style>

  <div class="container">
     <div class="box box-primary">
<!--           <h2> &nbsp; &nbsp; ServiceList</h2>
 -->

      <div class="box-body">
        
      @foreach ($errors->all() as $error)
        <p class="alert alert-danger">{{ $error }}</p>
      @endforeach
      @if(session('status'))
        <div class="alert alert-success">
        {{ session('status') }}
        </div>
      @endif
        

      @foreach ($pancarddata as $key => $value) 

            
      <!-- <form action="{{ url('/admin/service_list') }}" enctype="multipart/form-data" method="post">
      <input type="hidden" name="_token" value="{!! csrf_token() !!}">
      <input type="hidden" name="sid" value="{{ $value->id }}"> -->

      <div class="card">
        <div class="container">

              <div class="col-md-9">
                <div class="form-group">
                  <label for="aboutus_desc" class="col-lg-5 control-label"><dt> Date (Details saved) </dt></label>
              
                  <input type="text" readonly class="form-control" id="servicename" placeholder="Data entered date" name="servicename" value="{{$value->created_at}}">
                </div>

               </div>

           @if(isset($value->username))
              <div class="col-md-9">
                <div class="form-group">
                  <label for="aboutus_desc" class="col-lg-5 control-label"><dt> User Name </dt></label>
              
                  <input type="text" readonly class="form-control" id="servicename" placeholder="Operator Name" name="servicename" value="{{$value->username}} - {{ $value->mobile_no }}">
                </div>

               </div>
            @endif

             @if(isset($value->city))
              <div class="col-md-9">
                <div class="form-group">
                  <label for="aboutus_desc" class="col-lg-5 control-label"><dt> City </dt></label>
              
                  <input type="text" readonly class="form-control" id="servicename" placeholder="Operator Name" name="servicename" value="{{$value->city}}">
                </div>

               </div>
            @endif

           

             </div></div>      

         @endforeach     

            

      </div>

      

    

     </div>
    </div>

@stop