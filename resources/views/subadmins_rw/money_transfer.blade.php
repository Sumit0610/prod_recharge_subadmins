@extends('adminlte::page')

@section('title', 'APG Online Services')

@section('content_header')
    <h1><b>Money Transfer</b></h1>
@stop    

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style type="text/css"><style>
.card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 50%;
}

.card:hover {
    box-shadow: 0 0 50px 0 rgba(0,0,0,0.2);
}

.container {
    padding: 2px 16px;
    padding-top: 20px;
    padding-bottom: 0px;
}
.hideit
{
    display: none;
}
</style>

@section('wallet_balance')
           <a href="{{ url('admin/request_deposit') }}"><p style="float: right; font-size: 15px; margin-top: 10px; margin-bottom: 10px; margin-left: 10px;"><b>Request Deposit</b></p></a>

        <p style="float: right; font-size: 15px; margin-bottom: 10px; margin-top: 10px;"><b>Balance: ₹{{$userdata->wallet_balance}}</b></p>

@stop


@section('content')

    @if($userdata->iseko_verified == 1)
	<div class="container">
     <div class="box box-primary" style="padding: bottom: 0px;">
<!--           <h2> &nbsp; &nbsp;ABOUT US</h2>
 -->
          
		  	@foreach ($errors->all() as $error)
				<p class="alert alert-danger">{{ $error }}</p>
			@endforeach
			@if(session('status'))
				<div class="alert alert-success">
				{{ session('status') }}
				</div>
			@endif
            @if(session('error'))
                <div class="alert alert-danger">
                {{ session('error') }}
                </div>
            @endif
            @if(session('status1'))
                <div class="alert alert-danger">
                {{ session('status1') }} . 
                <script stype="text/javascript">
                    $(document).ready(function() {
                        $(".hideit").show('slow');
                    });
                </script>
                </div>
            @endif

            @php
                $recipient_data = session('recipient_data');
                $userdata = session('userdata');
                $sendercontact = session('sendercontact');
                $recipient_data = session('recipient_data');
                $mobile = session('mobile');
            @endphp

            <div class="col-md-12 box-body" style="width: 100%; margin-top: 20px; ">

                <!-- <div class="col-lg-3 col-sm-6">
                <div class="small-box bg-aqua">
                <div class="inner">
                <h4><b>Add Benificiary</b></h4>
                <p>Add &amp; send money</p>
                </div>
                <div class="icon" style="margin-top: 10px;">
                <i class="fa fa-user-circle"></i>
                </div>
                <button id="add_recipients" class="small-box-footer" style="background: #1D9D73; width: 100%;">Click to add benificiary &nbsp;<i class="fa fa-arrow-circle-right"></i></button>
                </div>
                </div>

                <div class="col-lg-3 col-sm-6">
                <div class="small-box bg-aqua">
                <div class="inner">
                <h4><b>View Benificiaries</b></h4>
                <p>View Benificiaries</p>
                </div>
                <div class="icon" style="margin-top: 10px;">
                <i class="fa fa-user-circle"></i>
                </div>
                <button id="view_recipients" class="small-box-footer" style="background: #1D9D73; width: 100%;">Click to view benificiaries &nbsp;<i class="fa fa-arrow-circle-right"></i></button>
                </div>
                </div>


                <div class="col-lg-3 col-sm-6">
                <div class="small-box bg-aqua">
                <div class="inner">
                <h4><b>Send money</b></h4>

                <p>Send money</p>
                </div>
                <div class="icon" style="margin-top: 10px;">
                <i class="fa fa-paper-plane"></i>
                </div>
                <button id="send_money" class="small-box-footer" style="background: #1D9D73; width: 100%;">Click to send money &nbsp;<i class="fa fa-arrow-circle-right"></i></button>
                </div>
                </div> -->

            </div>
 				
            <div id="default_hide_addrecipients" style="display:none;" class="wow fadeInUp animated box-body" data-wow-delay=".3s" data-wow-duration="500ms"> 

            <form method="POST" action="{{ URL('admin/add_recipients') }}"> 
                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                <div class="form-group float-label-control">
                <label for="">Benificiary Bank Account Number</label>
                <input type="text" id="account_number" name="account_number" class="form-control" placeholder="Enter Benificiary Bank Account Number">
                </div>
                <div class="form-group float-label-control">
                <label for="">Benificiary Bank IFSC Code</label>
                <input type="text" id="ifsc_code" name="ifsc_code" class="form-control" placeholder="Enter Benificiary Bank IFSC Code">
                </div>
                <div class="form-group float-label-control">
                <label for="">Benificiary Name</label>
                <input type="text" id="rec_name" name="rec_name" class="form-control" placeholder="Enter Benificiary Name">
                </div>
                <div class="form-group float-label-control">
                <label for="">Benificiary Contact</label>
                <input type="text" id="rec_mobile" name="rec_mobile" class="form-control" placeholder="Enter Benificiary Contact">
                </div>
                <div class="form-group float-label-control">
                <input type="submit" class="form-control" value="Add Benificiary" style="background: #00B9F5; color: white;">
                </div>
            </form>

             
            </div>



            <div id="default_hide_viewrecipients" style="display:none;" class="wow fadeInUp animated box-body" data-wow-delay=".3s" data-wow-duration="500ms"> 
                 @if(isset($recipient_data) && $recipient_data!="notexist")
                @foreach($recipient_data as $key => $value)

                    <div class="card">
                <div class="container">
                    
                    
             @if(isset($value->rec_name))
                <div class="col-md-8">
                    <div class="form-group">
                      <label for="aboutus_desc" class="col-lg-5 control-label"><dt> Benificiary Name </dt></label>
                  
                      <input type="text" disabled class="form-control" id="servicename" placeholder="Recipient Name" name="servicename" value="{{$value->rec_name}}">
                    </div>

                 </div>
              @endif

              @if(isset($value->rec_mobile))
                 <div class="col-md-8">
                    <div class="form-group">
                      <label for="aboutus_desc" class="col-lg-5 control-label"><dt> Benificiary Mobile No </dt></label>
                  
                      <input type="text" disabled class="form-control" id="servicedesc" placeholder="Recipient Mobile No" name="servicedesc" value="{{$value->rec_mobile}}">
                    </div>

                 </div>
               @endif

               @if(isset($value->account_number))
                 <div class="col-md-8">
                    <div class="form-group">
                      <label for="aboutus_desc" class="col-lg-5 control-label"><dt> Benificiary Account Number </dt></label>
                  
                      <input type="text" disabled class="form-control" id="servicedesc" placeholder="Account Number" name="servicedesc" value="{{$value->account_number}}">
                    </div>
                    <hr size="100">

                 </div>


               @endif   

               @if(isset($value->ifsc_code))
                 <div class="col-md-8">
                    <div class="form-group">
                      <label for="aboutus_desc" class="col-lg-5 control-label"><dt> IFSC code </dt></label>
                  
                      <input type="text" disabled class="form-control" id="servicedesc" placeholder="IFSC code" name="servicedesc" value="{{$value->ifsc_code}}">
                    </div>
                    <hr size="100">

                 </div>


               @endif   

               </div>
               </div>   

                               <hr style="height: 1px; border:none; background: #000000;"/>


                @endforeach
                @endif

            </div>

            
            <div id="sendmoney"  class="wow fadeInUp animated box-body" data-wow-delay=".3s" data-wow-duration="500ms"> 

            <div class="row">
            <div class="col-md-8">    
            <form method="POST" action="{{ URL('admin/send_mymoney') }}"> 
                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                <div class="form-group float-label-control">
                <label for="">Sender Contact Number</label>
                <input type="number" id="rec_mobile" name="rec_mobile" class="form-control" placeholder="Enter Sender Contact" maxlength="10">
                </div>
                
                <div class="form-group float-label-control">
                <input type="submit" class="form-control" value="Send Money" style="background: #00B9F5; color: white;">
                </div>
            </form>

            
                <div class="row hideit">
                    <div class="col-lg-8">
                        <form id="add-sender" action="{{ URL('admin/addsender') }}" method="post" role="form">
                            
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            
                            <div class="form-group">
                                <label for="sender_mobile">Sender Name</label>
                                <input type="text" name="sender_name" id="sender_name" class="form-control" placeholder="Enter Sender Name">
                            </div>

                            <div class="form-group">
                                <label for="sender_mobile">Sender Mobile Number</label>
                                <input type="text" name="sender_mobile" id="sender_mobile" class="form-control" placeholder="Enter Sender Mobile Number">
                            </div>

                            <div class="form-group">
                                <div class="form-group float-label-control">
                                    <input type="submit" name="register-submit" class="form-control" value="Save this sender" style="background: #00B9F5; color: white;">
                                </div>
                            </div>
                        </form>
                        
                    </div>
                </div>



            </div>
            
            @if(isset($recipient_data) && $recipient_data!="notexist")
                <div class="col-md-4">
                        <form action="{{ URL('admin/add_recipient') }}" method="get">
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="mobile" value="{{ $sendercontact }}">
                            <p>To add more Benificiary for sender {{$sendercontact}}. 
                                <button type="submit" class="btn btn-primary btn-sm">Click here</button>
                            to add Benificiery.</p>
                        </form>

                        <div class="form-group float-label-control">
                                <label for="">Benificiary</label>
                                </div>
                        @foreach($recipient_data as $rec)
                            <p>Name - {{$rec->recipient_name}} <br>
                            Mobile - {{$rec->recipient_mobile}}</p>
                            
                            <form method="get" action="{{ URL('admin/send_money_torecipient') }}"> 
                                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                    
                                <input type="hidden" name="recipient_name" id="recipient_name" value="{{ $rec->recipient_name }}">
                                
                                <input type="hidden" name="recipient_contact" id="recipient_contact" value="{{ $rec->recipient_mobile }}">
                                <input type="hidden" name="recipient_id" id="recipient_id" value="{{ $rec->recipient_id }}">
                                
                                <input type="hidden" name="sendercontact" id="sendercontact" value="{{ $sendercontact }}">
                                
                                <div class="form-group float-label-control">
                                <input type="submit" class="btn btn-sm" value="Transfer Money" style="background: #00B9F5; color: white;">
                                <hr>
                                </div>
                            </form>
                        @endforeach

                        
                    
                </div>
                
            @elseif($recipient_data=="notexist")
                <form action="{{ URL('admin/add_recipient') }}" method="get">
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="mobile" value="{{ $mobile }}">
                    <p>Benificiary does not exist for sender {{$mobile}}. 
                        <button type="submit" class="btn btn-primary btn-sm">Click here</button>
                    to add Benificiery</p>
                </form>
                
            @else
                <p>Benificiary does not exist.</p>
            @endif

             
            </div>
            


     </div>
    </div>
    @else

        <div class="container">
            <div class="box box-primary" style="padding: 5px;">

            <h3>Kindly verify your account with EKO first!</h3>

            </div>
        </div>    


    @endif
        
    <script type="text/javascript">
        
        $(document).ready(function(){

            $("#add_recipients").click(function(){
                $("#default_hide_addrecipients").show();
                $("#default_hide_viewrecipients").hide();
                $("#sendmoney").hide();
            });

            $("#view_recipients").click(function(){
                $("#default_hide_addrecipients").hide();
                $("#default_hide_viewrecipients").show();
                $("#sendmoney").hide();
            });

            $("#send_money").click(function(){
                $("#default_hide_addrecipients").hide();
                $("#default_hide_viewrecipients").hide();
                $("#sendmoney").show();
            });

            $("#clicktoaddbenificiery").click(function(event) {
                /* Act on the event */
                event.preventDefault();
                $("#default_hide_addrecipients").show();
                $("#default_hide_viewrecipients").hide();
                $("#sendmoney").hide();
            });

        });

    </script>    
    

      

@stop