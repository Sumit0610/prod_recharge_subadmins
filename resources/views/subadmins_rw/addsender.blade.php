@extends('adminlte::page')

@section('title', 'APG Online Services')

@section('content_header')
    <h1><b>Add sender</b></h1>
@stop    

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style type="text/css"><style>
.card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 50%;
}

.card:hover {
    box-shadow: 0 0 50px 0 rgba(0,0,0,0.2);
}

.container {
    padding: 2px 16px;
    padding-top: 20px;
    padding-bottom: 0px;
}
</style></style>


@section('content')

	<div class="container">
     <div class="box box-primary" style="padding: bottom: 0px;">
<!--           <h2> &nbsp; &nbsp;ABOUT US</h2>
 -->

            @if(session('status'))
                <div class="alert alert-success">
                {{ session('status') }}
                </div>
            @endif

            @if(session('error'))
                <div class="alert alert-danger">
                {{ session('error') }}
                </div>
            @endif                

                        <div class="row box-body">
                            <div class="col-lg-10">
                                    <form id="add-sender" action="{{ URL('admin/addsender') }}" method="post" role="form">
                                    
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    
                                    <div class="form-group">
                                        <label for="sender_mobile">Sender Name</label>
                                        <input type="text" name="sender_name" id="sender_name" class="form-control" placeholder="Enter Sender Name">
                                    </div>

                                    <div class="form-group">
                                        <label for="sender_mobile">Sender Mobile Number</label>
                                        <input type="text" name="sender_mobile" id="sender_mobile" class="form-control" placeholder="Enter Sender Mobile Number">
                                    </div>

                                    <div class="form-group">
                                        <div class="form-group float-label-control">
                                            <input type="submit" name="register-submit" class="form-control" value="Save this sender" style="background: #00B9F5; color: white;">
                                        </div>
                                    </div>
                                </form>
                                
                            </div>
                        </div>
          
        </div></div>
    
    

      

@stop