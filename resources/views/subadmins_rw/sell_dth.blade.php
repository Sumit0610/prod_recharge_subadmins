@extends('adminlte::page')

@section('title', 'Admin LTE')

@section('content_header')

    <h1><b>Sell DTH</b></h1>
@stop    

@section('wallet_balance')
           <a href="{{ url('admin/request_deposit') }}"><p style="float: right; font-size: 15px; margin-top: 10px; margin-bottom: 10px; margin-left: 10px;"><b>Request Deposit</b></p></a>

        <p style="float: right; font-size: 15px; margin-bottom: 10px; margin-top: 10px;"><b>Balance: ₹{{$userdata->wallet_balance}}</b></p>

@stop

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style type="text/css"><style>
.card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 50%;
}

.card:hover {
    box-shadow: 0 0 50px 0 rgba(0,0,0,0.2);
}

.container {
    padding: 2px 16px;
    padding-top: 20px;
    padding-bottom: 0px;
}
</style></style>


@section('content')

	<div class="container">
     <div class="box box-primary" style="padding: bottom: 0px;">
<!--           <h2> &nbsp; &nbsp;ABOUT US</h2>
 -->

            @if(session('status'))
                <div class="alert alert-success">
                {{ session('status') }}
                </div>
            @endif

            @if(session('error'))
                <div class="alert alert-danger">
                {{ session('error') }}
                </div>
            @endif                

                        <div class="row box-body">
                            <div class="col-lg-10">
                                    <form id="register-form" action="{{ URL('admin/save_sell_dth') }}" method="post" role="form">
                                     <div class="form-group col-md-8">
                                     <label for="operator">DTH Operator</label>
                                    <select name="operator" id="operator" class="col-md-12">
                                    <option value="NA" selected>-------Select operator-------</option>

                                        @foreach($dthdata as $key => $value) 

                                            <option value="{{ $value->id }}">
                                            {{ $value->operator_name }}
                                            </option>

                                        @endforeach

                                        <!-- <option value="0">Airtel HD</option>
                                        <option value="1">Airtel SD</option>
                                        <option value="2">Dish TV HD</option>
                                        <option value="3">Dish TV SD</option>
                                        <option value="4">TATA SKY HD</option>
                                        <option value="5">TATA SKY SD</option>
                                        <option value="6">VIDEOCON D2H HD</option>
                                        <option value="7">VIDEOCON D2H SD</option> -->
                                    </select>
                                    </div>
                                    <input id="make_text" type = "hidden" name = "make_text" value = "" />

                                    <div class="form-group col-md-8">
                                        <input type="text" name="rate" id="rate" tabindex="1" class="form-control" placeholder="Rate" readonly>
                                    </div>
                                    <div class="form-group col-md-8">
                                        <input type="text" name="commission" id="commission" tabindex="1" class="form-control" placeholder="Commission" readonly>
                                    </div>
                                    <div class="form-group col-md-8">
                                        <input type="text" name="amount" id="amount" tabindex="1" class="form-control" placeholder="Amount" readonly>
                                    </div>
                                    <div class="form-group col-md-8">
                                        <input type="text" name="buyername" id="buyername" tabindex="1" class="form-control" placeholder="Buyer Name" value="" required>
                                    </div>
                                    <div class="form-group col-md-8">
                                        <input type="text" name="installation_address" id="installation_address" tabindex="1" class="form-control" placeholder="Installation Address" value="" required>
                                    </div>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="userid" value="{{ $userid }}">

                                    <div class="form-group col-md-8">
                                        <input type="text" name="city" id="city" tabindex="1" class="form-control" placeholder="City" value="" required>
                                    </div>

                                    <div class="form-group col-md-8">
                                        <input type="number" name="pincode" id="pincode" tabindex="1" class="form-control" placeholder="Pincode" value="" required>
                                    </div>

                                    <div class="form-group col-md-8">
                                        <input type="number" name="mobile_no" id="mobile_no" tabindex="1" class="form-control" placeholder="Customer Mobile Number" value="" required>
                                    </div>

                                    <div class="form-group col-md-8">
                                        <input type="number" name="altmobile_no" id="altmobile_no" tabindex="1" class="form-control" placeholder="Customer Mobile Number(Alternate if any)" value="">
                                    </div>
                                    
                                    <div class="form-group col-md-8">
                                    <div class="form-group float-label-control">
                                    <input type="submit" name="register-submit" class="form-control" value="Save details" style="background: #00B9F5; color: white;">
                                    </div></div>
                                </form>
                                
                            </div>
                        </div>
          
        </div></div>


        <script type="text/javascript">
            
            $('select').on('change', function() {

                var thisval = this.value;
   
                if(this.value == "NA") {
                    $('#rate').val("Select an operator from the list");
                    $('#commission').val("Select an operator from the list");
                    $('#amount').val("Select an operator from the list");
                    $('#make_text').val(this.options[this.selectedIndex].text);
                }
                else {
                $('#make_text').val(this.options[this.selectedIndex].text);
                   $.ajax({
                    url: "getamountdetails",
                    type: "POST",
                    data: {"dthid":this.value, "_token": "{{ csrf_token() }}"},
                    success: function (data) {
                         var respjson = JSON.parse(data);
                         var amounts = respjson.amount;  
                         var rate = respjson.rate;
                         var commission = respjson.commission;

                    
                    $('#rate').val(rate);
                    $('#commission').val(commission);
                    $('#amount').val(amounts);
                    }
                });
                   
                }
            })

        </script>
    

      

@stop