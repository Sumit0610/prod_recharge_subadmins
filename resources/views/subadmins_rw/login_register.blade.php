@extends('adminlte::page')

@section('title', 'APG Online Services')

@section('content_header')
    <h1><b>Money Transfer</b></h1>
@stop    

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style type="text/css"><style>
.card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 50%;
}

.card:hover {
    box-shadow: 0 0 50px 0 rgba(0,0,0,0.2);
}

.container {
    padding: 2px 16px;
    padding-top: 20px;
    padding-bottom: 0px;
}
</style></style>


@section('content')

	<div class="container">
     <div class="box box-primary" style="padding: bottom: 0px;">
<!--           <h2> &nbsp; &nbsp;ABOUT US</h2>
 -->

            @if(session('status'))
                <div class="alert alert-success">
                {{ session('status') }}
                </div>
            @endif

            @if(session('error'))
                <div class="alert alert-danger">
                {{ session('error') }}
                </div>
            @endif                

                        <div class="row box-body">
                            <div class="col-lg-10">

                                    <form id="register-form" action="{{ URL('admin/save_website_user') }}" method="post" role="form">
                                    <div class="form-group">
                                        <input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Name" value="{{ $userdatas->name }}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <input type="email" name="email" id="email" tabindex="1" class="form-control" placeholder="Email Address" value="{{ $userdatas->email }}" readonly>
                                    </div>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-group">
                                        <input type="number" name="mobile_no" id="mobile_no" tabindex="1" class="form-control" placeholder="Mobile Number" value="{{ $userdatas->user_phone }}" readonly>
                                    </div>
                                     
                                    
                                        <div class="form-group">
                                                <div class="form-group float-label-control">
                    <input type="submit" name="register-submit" class="form-control" value="Verify!" style="background: #00B9F5; color: white;">
                    </div>
                                    </div>
                                </form>
                                
                            </div>
                        </div>
          
        </div></div>
    
    

      

@stop