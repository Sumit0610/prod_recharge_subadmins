@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <!-- <h1>Dashboard</h1> -->

        <h1><b>Home</b></h1>


<style type="text/css">
	
.img1 {
  transition: 1.70s;
  -webkit-transition: 1.70s;
  -moz-transition: 1.70s;
  -ms-transition: 1.70s;
  -o-transition: 1.70s;
  display: block;
  margin-right: auto;
  margin-left: auto;
}

.img1:hover {
  transition: 1.70s;
  -webkit-transition: 1.70s;
  -moz-transition: 1.70s;
  -ms-transition: 1.70s;
  -o-transition: 1.70s;
  -webkit-transform: rotate(360deg);
  -moz-transform: rotate(360deg);
  -o-transform: rotate(360deg);
  -ms-transform: rotate(360deg);
  transform: rotate(360deg);
} 

#div_circles_image {width:100%; margin:0 auto 0 auto; text-align:center;}
#div_circles_image div
{
display:inline-block;
padding: 5px;
border-radius: 50%;
width: 60px;
height: 60px;
background: #E0FFFF;
margin: 5px;
}

.marquee {
    width: 100%;
	line-height: 50px;
	background-color: white;
	color: black;
    white-space: nowrap;
    overflow: hidden;
    box-sizing: border-box;
}
.marquee p {
    display: inline-block;
    padding-left: 100%;
    animation: marquee 15s linear infinite;
}
@keyframes marquee {
    0%   { transform: translate(0, 0); }
    100% { transform: translate(-100%, 0); }
}

</style>


@stop

@section('wallet_balance')
           <a href="{{ url('admin/request_deposit') }}"><p style="float: right; font-size: 15px; margin-top: 10px; margin-bottom: 10px; margin-left: 10px;"><b>Request Deposit</b></p></a>

        <p style="float: right; font-size: 15px; margin-bottom: 10px; margin-top: 10px;"><b>Balance: ₹{{ $userdata->wallet_balance }}</b></p>

@stop

@section('content')

<div class="container">
     <div class="box box-primary" style="padding: 10px;">

     	<h4><b>Services</b></h4> <hr>

			@foreach ($errors->all() as $error)
				<p class="alert alert-danger">{{ $error }}</p>
			@endforeach
			@if(session('status'))
				<div class="alert alert-success">
				{{ session('status') }}
				</div>
			@endif
            @if(session('error'))
                <div class="alert alert-danger">
                {{ session('error') }}
                </div>
            @endif


            <div id="div_circles_image" style="padding-bottom: 10px;">

				<div><a href="{{URL('admin/recharge_phone')}}"><img class="img1"  src="{{ asset('/uploads/dashboard_icons/mobile.png') }}" style="width:50px;" class="img-responsive"></a><b>MOBILE</b></div>
				<div><a href="{{URL('admin/money_transfer')}}"><img class="img1"  src="{{ asset('/uploads/dashboard_icons/rupee.png') }}" style="width:50px;" class="img-responsive"></a><b>MONEY</b></div>
				<div><a href="#"><img class="img1"  src="{{ asset('/uploads/dashboard_icons/tower.png') }}" style="width:50px;" class="img-responsive"></a><b>LIGHT</b></div>
				<div><a href="http://23.92.20.71/documents/Form6.pdf" target="_blank"><img  class="img1" src="{{ asset('/uploads/dashboard_icons/election.png') }}" style="width:50px;" class="img-responsive"></a><b>VOTER</b></div>
				<div><a href="{{ URL('admin/pancard') }}"><img class="img1"  src="{{ asset('/uploads/dashboard_icons/nsdl.gif') }}" style="width:50px;" class="img-responsive"></a><b>PAN</b></div>
				<div><a href="#"><img class="img1"  src="{{ asset('/uploads/dashboard_icons/bus.png') }}" style="width:50px;" class="img-responsive"></a><b>BUS</b></div>
				<div><a href="#"><img class="img1"  src="{{ asset('/uploads/dashboard_icons/flight.png') }}" style="width:50px;" class="img-responsive"></a><b>FLIGHT</b></div>
				<div><a href="#"><img class="img1"  src="{{ asset('/uploads/dashboard_icons/hotel.png') }}" style="width:50px;" class="img-responsive"></a><b>HOTEL</b></div>
				<div><a href="#"><img  class="img1" src="{{ asset('/uploads/dashboard_icons/reports.png') }}" style="width:50px;" class="img-responsive"></a><b>REPORTS</b></div>
				<div><a href="#"><img class="img1"  src="{{ asset('/uploads/dashboard_icons/app.png') }}" style="width:50px;" class="img-responsive"></a><b>APP</b></div>
			</div>	

</div>

<div class="box box-primary" style="padding: 10px;">

			@foreach ($errors->all() as $error)
				<p class="alert alert-danger">{{ $error }}</p>
			@endforeach
			@if(session('status'))
				<div class="alert alert-success">
				{{ session('status') }}
				</div>
			@endif
            @if(session('error'))
                <div class="alert alert-danger">
                {{ session('error') }}
                </div>
            @endif

     	<h4><b>Latest News</b></h4> <hr>

     	<div class="marquee">

     	<p>@foreach($newsdata as $key => $value){{ $value->news_desc }} &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;@endforeach</p>

     	</div>

</div>


</div>


@stop