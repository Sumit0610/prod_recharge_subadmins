@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><b>Send money</b></h1>
@stop

@section('content')

<style>
.card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 100%;
}

.card:hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

.container {
    padding: 2px 16px;
    padding-top: 20px;
}
</style>

  <div class="container">
     <div class="box box-primary">
<!--           <h2> &nbsp; &nbsp; ServiceList</h2>
 -->

      <div class="box-body">
        
      @foreach ($errors->all() as $error)
        <p class="alert alert-danger">{{ $error }}</p>
      @endforeach
      @if(session('status'))
        <div class="alert alert-success">
        {{ session('status') }}
        </div>
      @endif

      @if(session('error'))
        <div class="alert alert-danger">
        {{ session('error') }}
        </div>
      @endif
          
    
    <form method="post" action="{{URL('admin/send_money')}}">
     <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
     <input type="hidden" name="eko_recipient_id" id="eko_recipient_id" value="{{$recipient_id}}">

      <div class="card">
        <div class="container">
 

           @if(isset($sendercontact))
               <div class="col-md-8">
                <div class="form-group">
                  <label for="subject_message" class="col-lg-5 control-label"><dt> Sender Contact </dt></label>
              
                  <input type="text" readonly class="form-control" id="sender_mobile" placeholder="User Subject" name="sender_mobile" value="{{$sendercontact}}">
                </div>

               </div>


             @endif 
             
           @if(isset($recipient_name))
        <div class="col-md-8">
                <div class="form-group">
                  <label for="aboutus_desc" class="col-lg-5 control-label"><dt> Benificiary Name </dt></label>
              
                  <input type="text" readonly class="form-control" id="benificiaryname" placeholder="Name" name="benificiaryname" value="{{$recipient_name}}">
                </div>

               </div>
            @endif

             

             @if(isset($reccontact))
               <div class="col-md-8">
                <div class="form-group">
                  <label for="subject_message" class="col-lg-5 control-label"><dt> Benificiary Contact </dt></label>
              
                  <input type="text" readonly class="form-control" id="rec_mobile" placeholder="User Subject" name="rec_mobile" value="{{$reccontact}}">
                </div>

               </div>


             @endif 

             
               <div class="col-md-8">
                <div class="form-group">
                  <label for="subject_message" class="col-lg-5 control-label"><dt>  Amount (Minimum-100rs)</dt></label>
              
                  <input type="text" class="form-control" id="amount" placeholder="Enter amount to transfer" name="amount" >
                </div>

               </div>

              <div class="col-md-8">
                <div class="form-group">
                  <label for="subject_message" class="col-lg-5 control-label"><dt> For Verification</dt></label>
              
                  <select type="text" class="form-control" id="verification" name="verification">
                    <option value="1">Aadhar card</option>
                    <option value="2">Pan card</option>
                  </select>
                </div>

               </div>

               <div class="col-md-8">
                <div class="form-group">
                  <label for="subject_message" class="col-lg-5 control-label"><dt>Adharcard Number / Pan card Number</dt></label>
              
                  <input type="text" class="form-control" id="verification_number" name="verification_number">
                </div>

               </div>


              <div class="col-md-8">
                <div class="form-group">
                  <label for="subject_message" class="col-lg-5 control-label"><dt> Transfer Mode</dt></label>
              
                  <select class="form-control" id="transfermode" name="transfermode">
                    <option value="2">IMPS</option>
                    <option value="1">NEFT</option>
                  </select>
                </div>

               </div>

               <div class="col-md-8">
                <div class="form-group">
              
                  <input type="submit" class="brn btn-primary" id="sendmymoney" name="sendmymoney" value="Send money">
                </div>

               </div>

             

             </div></div>      
    
          </form>
            

      </div>

      

    

     </div>
    </div>

@stop