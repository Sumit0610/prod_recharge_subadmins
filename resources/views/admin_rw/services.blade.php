@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><b>Add Service details</b></h1>
@stop

@section('content')


<html lang="en">
<head>
  <title></title>
  <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.5.1/croppie.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.5.1/croppie.css">
</head>


<body>
<div class="container">
    <div class="panel panel-default">
      <div class="panel-heading"></div>
      <div class="panel-body">


             @foreach ($errors->all() as $error)
                <p class="alert alert-danger">{{ $error }}</p>
            @endforeach
            @if(session('status'))
                <div class="alert alert-success">
                {{ session('status') }}
                </div>
            @endif


        <div class="row">
            
            
             <div class="col-md-12">
                <div class="form-group">
                  <label for="aboutus_desc" class="col-lg-5 control-label"><dt>Service Name</dt></label>
              
                  <input type="text" class="form-control" id="teammemnm" placeholder="Enter service name" name="teammemnm">
                </div>

             </div>

             <div class="col-md-12">
                <div class="form-group">
                  <label for="aboutus_desc" class="col-lg-5 control-label"><dt>Service Short Description</dt></label>
              
                  <input type="text" class="form-control" id="teammempos" placeholder="Enter service description in short" name="teammempos">
                </div>

             </div>

             <div class="col-md-12">
                <div class="form-group">
                  <label for="service_brief" class="col-lg-5 control-label"><dt>Service Brief Description</dt></label>
              
                  <input type="text" class="form-control" id="service_brief" placeholder="Enter service description in brief" name="service_brief">
                </div>

             </div>

             <div class="text-center">
                <div id="upload-demo" style="margin-top: 20px;"></div>
            </div>

            <div class="col-md-12" style="padding-top:10px;">
                <strong>Select Image:</strong>
                <br/>
                <input type="file" id="upload">
                <br/>
                <button class="btn btn-success upload-result">Save details</button>
            </div>

   


            <div class="col-md-12" style="">
                <div id="upload-demo-i" style="padding:30px;margin-top:30px; display:none;"></div>
            </div>
        </div>


      </div>
    </div>
</div>


<script type="text/javascript">

var sayt=false;
$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});


$uploadCrop = $('#upload-demo').croppie({
    enableExif: true,
    viewport: {
        width: 380,
        height: 250,
        type: 'square'
    },
    boundary: {
        width: 385,
        height: 255
    }
});


$('#upload').on('change', function () { 
    var reader = new FileReader();
    reader.onload = function (e) {
        $uploadCrop.croppie('bind', {
            url: e.target.result
        }).then(function(){
            console.log('jQuery bind complete');
            sayt = true;
        });
    }
    reader.readAsDataURL(this.files[0]);
});


$('.upload-result').on('click', function (ev) {
    if(sayt)
    {
        var tname = $("#teammemnm").val();
        var tpos = $("#teammempos").val();
        var service_brief = $("#service_brief").val();
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (resp) {
            $.ajax({
               
                url: "service_add",
                type: "POST",
                data: {"image":resp,"tnm":tname, "tps":tpos, "service_brief":service_brief , "_token":"{{ csrf_token() }}"},
                success: function (res) {

                 html = '<img src="' + resp + '" />';
                    $("#upload-demo-i").html(html);
                    
                    setTimeout(function() { alert("Service added successfully"); 
                    location.reload();}, 1000);
                    
                    
                    <!-- document.write(JSON.stringify(res)); -->
                   
                }
            });
        });
    }
});


</script>


</body>
</html>

@stop