@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><b>Queries List</b></h1>
@stop

@section('content')

<style>
.card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 100%;
}

.card:hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

.container {
    padding: 2px 16px;
    padding-top: 20px;
}
</style>

	<div class="container">
     <div class="box box-primary">
<!--           <h2> &nbsp; &nbsp; ServiceList</h2>
 -->

			<div class="box-body">
 				
				@foreach ($errors->all() as $error)
				<p class="alert alert-danger">{{ $error }}</p>
			@endforeach
			@if(session('status'))
				<div class="alert alert-success">
				{{ session('status') }}
				</div>
			@endif
 				

 			@foreach ($data_contactus as $key => $value) 
 						
 			<!-- <form action="{{ url('/admin/service_list') }}" enctype="multipart/form-data" method="post">
 			<input type="hidden" name="_token" value="{!! csrf_token() !!}">
 			<input type="hidden" name="sid" value="{{ $value->id }}"> -->

 			<div class="card">
 				<div class="container">
		  			
 					
        	 @if(isset($value->name))
 				<div class="col-md-8">
		            <div class="form-group">
		              <label for="aboutus_desc" class="col-lg-5 control-label"><dt> User Name </dt></label>
		          
		              <input type="text" disabled class="form-control" id="servicename" placeholder="Name" name="servicename" value="{{$value->name}}">
		            </div>

	             </div>
	          @endif

	          @if(isset($value->email))
	             <div class="col-md-8">
		            <div class="form-group">
		              <label for="aboutus_desc" class="col-lg-5 control-label"><dt> User Email </dt></label>
		          
		              <input type="text" disabled class="form-control" id="servicedesc" placeholder="User Email" name="servicedesc" value="{{$value->email}}">
		            </div>

	             </div>
	           @endif

	           @if(isset($value->subject_message))
	             <div class="col-md-8">
		            <div class="form-group">
		              <label for="subject_message" class="col-lg-5 control-label"><dt> User Message </dt></label>
		          
		              <input type="text" disabled class="form-control" id="subject_message" placeholder="User Subject" name="subject_message" value="{{$value->subject_message}}">
		            </div>
		            <hr size="100">

	             </div>


	           @endif	

	           @if(isset($value->message))
	             <div class="col-md-8">
		            <div class="form-group">
		              <label for="aboutus_desc" class="col-lg-5 control-label"><dt> User Message </dt></label>
		          
		              <input type="text" disabled class="form-control" id="servicedesc" placeholder="User Message" name="servicedesc" value="{{$value->message}}">
		            </div>
		            <hr size="100">

	             </div>


	           @endif	

	           </div></div>      

			   @endforeach     

	          

 			</div>

 			

 		

     </div>
    </div>

@stop