@extends('adminlte::page')

@section('title', 'APG Online Services')

@section('content_header')
    <h1><b>Recharge Reports</b></h1>
@stop    

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style type="text/css">
.card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 50%;
}

.card:hover {
    box-shadow: 0 0 50px 0 rgba(0,0,0,0.2);
}

.container {
    padding: 2px 16px;
    padding-top: 20px;
    padding-bottom: 0px;
}
</style>


@section('wallet_balance')
           <a href="{{ url('admin/request_deposit') }}"><p style="float: right; font-size: 15px; margin-top: 10px; margin-bottom: 10px; margin-left: 10px;"><b>Request Deposit</b></p></a>

        <p style="float: right; font-size: 15px; margin-bottom: 10px; margin-top: 10px;"><b>Balance: ₹{{$userdata->wallet_balance}}</b></p>

@stop


@section('content')


	<div class="container">
     <div class="box box-primary" style="padding: bottom: 0px;">
<!--           <h2> &nbsp; &nbsp;ABOUT US</h2>
 -->

        @if(isset($recharge_data))
          
		  	@foreach ($errors->all() as $error)
				<p class="alert alert-danger">{{ $error }}</p>
			@endforeach
			@if(session('status'))
				<div class="alert alert-success">
				{{ session('status') }}
				</div>
			@endif
            @if(session('error'))
                <div class="alert alert-danger">
                {{ session('error') }}
                </div>
            @endif
 				

            <div id="sendmoney"  class="wow fadeInUp animated box-body" data-wow-delay=".3s" data-wow-duration="500ms"> 

                
                <table class="table table-bordered table-responsive">
                    <thead>
                      <tr>
                        <th>Index</th>
                        <th>Transaction amount</th>
                        <th>Recharge status</th>
                        <th>Mobile Number</th>
                        <th>Operator</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($recharge_data as $recharge)
                      <tr>
                        <td>{{$i++}}</td>
                        <td>{{$recharge->recharge_amount}}</td>
                        <td>{{$recharge->recharge_done}}</td>
                        <td>{{$recharge->user_mobile}}</td>
                        <td>
                            @if($recharge->operator=="AC")
                            {{"Aircel"}}
                            @elseif($recharge->operator=="AT")
                            {{"Airtel"}}
                            @elseif($recharge->operator=="PA")
                            {{"Airtel(Postpaid)"}}
                            @elseif($recharge->operator=="PB")
                            {{"BSNL (Postpaid)"}}
                            @elseif($recharge->operator=="BT")
                            {{"BSNL (Topup)"}}
                            @elseif($recharge->operator=="BV")
                            {{"BSNL (Validity)"}}
                            @elseif($recharge->operator=="ID")
                            {{"Idea"}}
                            @elseif($recharge->operator=="PI")
                            {{"Idea (Postpaid)"}}
                            @elseif($recharge->operator=="PR")
                            {{"Reliance (Postpaid)"}}
                            @elseif($recharge->operator=="RC")
                            {{"Reliance CDMA"}}
                            @elseif($recharge->operator=="RG")
                            {{"Reliance GSM"}}
                            @elseif($recharge->operator=="PD")
                            {{"TATA Docomo (Postpaid)"}}
                            @elseif($recharge->operator=="TS")
                            {{"TATA Docomo (SP)"}}
                            @elseif($recharge->operator=="TN")
                            {{"TATA Docomo (Normal)"}}
                            @elseif($recharge->operator=="TI")
                            {{"Tata Indicom"}}
                            @elseif($recharge->operator=="UN")
                            {{"Uninor"}}
                            @elseif($recharge->operator=="US")
                            {{"Uninor (SP)"}}
                            @elseif($recharge->operator=="VI")
                            {{"Virgin/T24"}}    
                            @elseif($recharge->operator=="VS")
                            {{"Virgin/T24(SP)"}}    
                            @elseif($recharge->operator=="RV")
                            {{"Vodafone"}}    
                            @elseif($recharge->operator=="PV")
                            {{"Vodafone (Postpaid)"}}              
                            @endif
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                  

             
            </div>


            <div id="default_hide_viewrecipients"  class="wow fadeInUp animated box-body" data-wow-delay=".3s" style="display:none;" data-wow-duration="500ms"> 

                
                

             
            </div>


       @else 
       

        <h3>Please contact APG Online Services and get your account verified first!</h3>     

		   
       @endif     

     </div>
    </div>
        
    <script type="text/javascript">
        
        $(document).ready(function(){

            $("#add_recipients").click(function(){
                $("#default_hide_addrecipients").hide();
                $("#default_hide_viewrecipients").hide();
                $("#sendmoney").show();
            });

            $("#view_recipients").click(function(){
                $("#default_hide_addrecipients").hide();
                $("#default_hide_viewrecipients").show();
                $("#sendmoney").hide();
            });

            $("#send_money").click(function(){
                $("#default_hide_addrecipients").hide();
                $("#default_hide_viewrecipients").hide();
                $("#sendmoney").show();
            });

        }); 

    </script>    
    

      

@stop