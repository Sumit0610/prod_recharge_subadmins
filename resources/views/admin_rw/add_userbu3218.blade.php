@extends('adminlte::page')

@section('title', 'Admin LTE')

@section('content_header')

    <h1><b>Add Users (Master Distributors, Distrbutors, Retailers, etc.)</b></h1>
@stop    

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style type="text/css"><style>
.card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 50%;
}

.card:hover {
    box-shadow: 0 0 50px 0 rgba(0,0,0,0.2);
}

.container {
    padding: 2px 16px;
    padding-top: 20px;
    padding-bottom: 0px;
}
</style></style>


@section('content')

	<div class="container">
     <div class="box box-primary" style="padding: bottom: 0px;">
<!--           <h2> &nbsp; &nbsp;ABOUT US</h2>
 -->

            @if(session('status'))
                <div class="alert alert-success">
                {{ session('status') }}
                </div>
            @endif

            @if(session('error'))
                <div class="alert alert-danger">
                {{ session('error') }}
                </div>
            @endif                

                        <div class="row box-body">

                          @if($userdata->user_type == 2)
                            <h3>You cannot add any user! You are a retailer!</h3>
                          @else  
                            <div class="col-lg-10">
                                    <form id="register-form" action="{{ URL('admin/save_subadmin_user') }}" method="post" role="form">
                                    <div class="form-group">
                                        <input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Full Name" value="" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="email" name="email" id="email" tabindex="1" class="form-control" placeholder="Email Address" value="" required>
                                    </div>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="userid" value="{{ $userdata->id }}">
                                    <div class="form-group">
                                        <input type="number" name="mobile_no" id="mobile_no" tabindex="1" class="form-control" placeholder="Mobile Number" value="" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="address" id="address" tabindex="1" class="form-control" placeholder="Complete Address" value="" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="panno" id="panno" tabindex="1" class="form-control" placeholder="PAN Card No" value="" required>
                                    </div>
                                     <div class="form-group">
                                     <label for="type_customer" class="col-md-12">Customer type</label>
                                    <select name="type_customer" id="type_customer">
                                    <option value="NA">-------Select an option-------</option>
                                     
                                      @if($userdata->user_type == 0)
                                        <option value="1">Distributor</option>
                                        <option value="2">Retailer</option>
                                      @elseif($userdata->user_type == 1)
                                        <option value="2">Retailer</option>
                                      @endif  
                                    </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password">
                                    </div>
                                    
                                    <div class="form-group">
                                    <div class="form-group float-label-control">
                                    <input type="submit" name="register-submit" class="form-control" value="Add User" style="background: #00B9F5; color: white;">
                                    </div></div>
                                </form>
                                
                            </div>
                        @endif    
                        </div>
          
        </div></div>
    
    

      

@stop