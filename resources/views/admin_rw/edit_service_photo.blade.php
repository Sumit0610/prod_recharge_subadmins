@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><b>{{ $service_data->service_name }}'s photo update</b></h1>
@stop

@section('content')


<html lang="en">
<head>
  <title></title>
  <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.5.1/croppie.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.5.1/croppie.css">
</head>


<body>
<div class="container">
	<div class="panel panel-default">
	  <div class="panel-heading"></div>
	  <div class="panel-body">


	  	<div class="row">
	  		<div class="text-center">
				<div id="upload-demo" style="margin-top: 20px;"></div>
	  		</div>
	  		<div class="col-md-12" style="padding-top:10px;">
				<strong>Select Image:</strong>
				<br/>
				<input type="file" id="upload">
				<br/>
				<button class="btn btn-success upload-result">Upload Image</button>
	  		</div>


	  		<div class="col-md-12" style="">
				<div id="upload-demo-i" style="background:#e1e1e1;padding:30px;margin-top:30px; display:none;"></div>
	  		</div>
	  	</div>


	  </div>
	</div>
</div>


<script type="text/javascript">

var sayt=false;
$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});


$uploadCrop = $('#upload-demo').croppie({
    enableExif: true,
    viewport: {
        width: 380,
        height: 250,
        type: 'square'
    },
    boundary: {
        width: 385,
        height: 255
    }
});


$('#upload').on('change', function () { 
	var reader = new FileReader();
    reader.onload = function (e) {
    	$uploadCrop.croppie('bind', {
    		url: e.target.result
    	}).then(function(){
    		console.log('jQuery bind complete');
    		sayt = true;
    	});
    }
    reader.readAsDataURL(this.files[0]);
});


$('.upload-result').on('click', function (ev) {
	if(sayt)
	{
		$uploadCrop.croppie('result', {
			type: 'canvas',
			size: 'viewport'
		}).then(function (resp) {
			$.ajax({
				url: "edit_service_photo",
				type: "POST",
				data: {"image":resp, "id":"{{$id}}", "_token": "{{ csrf_token() }}"},
				success: function (data) {
					html = '<img src="' + resp + '" />';
					$("#upload-demo-i").html(html);
					alert('Image has been uploaded successfully');
					window.location.replace("{{url('/admin/service_update')}}");
				}
			});
		});
	}
});


</script>


</body>
</html>

@stop